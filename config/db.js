
// const {
//   Pool
// } = require('pg');
// const { Pool, createConnection} = require('mysql2');
const Sequelize = require('sequelize')
let db;


class DB {
  getDBConnect() {
    return new Promise(async (resolve, reject) => {
      try {
        db = new Sequelize('saloon', 'smit', 'tristate123', {
            host: 'localhost',
            dialect: 'postgres'
          });
        db.authenticate().then(() => {
            console.log('Connection has been established successfully.');
         
          return resolve();
        });
      } catch (error) {
        console.log(`\ngetDBConnect catch error ->> ${error}`);
        return reject(error);
      }
    });
  }
  
  custom(query) {
    return new Promise((resolve, reject) => {
      // console.log(`\nCustom query ->> ${query}`);

      db.query(query, (error, results) => {
        if (error) {
          console.log(`\nCustom error ->> ${error}`);
          return reject("SOMETHING_WENT_WRONG");
        } else {
          console.log(results)
          return resolve(results);
        }
      });
    });
  }


  

 
}

module.exports = new DB();