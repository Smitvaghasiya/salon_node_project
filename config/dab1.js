const Sequelize = require("sequelize");
let sequelize;
class Db {
  getDBConnect(DD) {
    return new Promise(async (resolve, reject) => {
      try {
        const sequelize = new Sequelize("saloon", "smit", "tristate123", {
          host: "localhost",
          dialect: "postgres",
          pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000,
          },

          // SQLite only

          // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
        //   operatorsAliases: false,
        });

        let DD=await sequelize.query("SELECT  (avg(price)+avg(value)+avg(quality)+avg(friendliness)+avg(cleanliness))/5 as avragerate from write_reviews where salon_id=34").then((results) => {
          console.log('results',results);
        });
        console.log(DD)
      } catch (error) {
        console.log(`\ngetDBConnect catch error ->> ${error}`);
        return reject(error);
      }
    });
  }

  custom(query) {
    return new Promise((resolve, reject) => {
      // console.log(`\nCustom query ->> ${query}`);

      sequelize.query(query, (error, results) => {
        if (error) {
          console.log(`\nCustom error ->> ${error}`);
          return reject("SOMETHING_WENT_WRONG");
        } else {
          console.log(results);
          return resolve(results);
        }
      });
    });
  }
}

module.exports=new Db();