const jwt = require("jsonwebtoken");
// const config = require('../config');
const fs = require("fs");
const db = require("../config/db");
const path = require("path");

class Common {
  getJwtToken(user_id) {
    return new Promise((resolve, reject) => {
      try {
        let expirationTime = config.jwtExpiryUserTime,
          sign = {
            user_id,
          };

        let token = jwt.sign(sign, config.jwtSecretKey, {
          expiresIn: expirationTime,
        });
        return resolve(token);
      } catch (error) {
        return reject(error);
      }
    });
  }

  generateOTP(n) {
    let digits = "0123456789";
    let otp = "";

    for (let i = 0; i < n; i++) {
      let index = Math.floor(Math.random() * digits.length);

      if (i == 0 && !parseInt(digits[index])) i--;
      else otp += digits[index];
    }

    return otp;
  }

  profile_pic(image) {
    let profile_pic = path.format({
      root: "/ignored",
      dir: "http://localhost:3000/images",
      base: image,
    });
    console.log("profile_pic", profile_pic);
    return profile_pic;
  }

  images_videos(image) {
    let images_videos = path.format({
      root: "/ignored",
      dir: "http://localhost:3000/image",
      base: image,
    });
    console.log("images_videos", images_videos);
    return images_videos;
  }

  print24(str) {
    // Get hours
    var h1 = Number(str[1] - "0");
    var h2 = Number(str[0] - "0");
    var hh = h2 * 10 + (h1 % 10);

    // If time is in "AM"
    if (str[5] == "A") {
      if (hh == 12) {
        console.log("00");
        for (var i = 2; i <= 7; i++) console.log(str[i]);
      } else {
        for (var i = 0; i <= 7; i++) console.log(str[i]);
      }
    }

    // If time is in "PM"
    else {
      if (hh == 12) {
        console.log("12");
        for (var i = 2; i <= 7; i++) console.log(str[i]);
      } else {
        hh = hh + 12;
        console.log(hh);
        for (var i = 2; i <= 7; i++) console.log(str[i]);
      }
    }
  }

  convertTimeFormat(time) {
    var hrs = Number(time.match(/^(\d+)/)[1]);
    var mnts = Number(time.match(/:(\d+)/)[1]);
    var format = time.match(/\s(.*)$/)[1];

    if (format == "PM" && hrs < 12) hrs = hrs + 12;
    if (format == "AM" && hrs == 12) hrs = hrs - 12;

    var hours = hrs.toString();
    var minutes = mnts.toString();

    if (hrs < 10) hours = "0" + hours;
    if (mnts < 10) minutes = "0" + minutes;

    var timeend = hours + ":" + minutes;
    console.log(timeend); //h:i:s
    return timeend;
  }

  generateCode(length) {
    const charset =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n)).toUpperCase();
    }

    return retVal;
  }

  // calling way

  // Driver code

  AddMinutesToDate(date, minutes) {
    return date.getHours() + minutes * 60000;
  }

  paginate(array1, size, page) {
    return array1.slice((page - 1) * size, page * size);
  }

  paginatedata(array_data, size, page) {
    return array_data.slice((page - 1) * size, page * size);
  }

  async pushNotification(user_id, type) {
    let device_token = await User_device.findOne({
      attributes: ["device_token"],
      where: { user_id },
    });
    console.log("device_token ==>>", device_token.device_token);

    let token = device_token.device_token;
    let notification = await notificationData(type);
    let data = { type };

    let message = await firebase.sendFCMPushNotification(
      token,
      notification,
      data
    );
    console.log("message ==>>", message);
    return;
  }

  async get_date(date) {
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

    if (day < 10) {
      day = "0" + day;
    }

    if (month + 1 < 10) {
      month = `0${month + 1}`;
    }
    let format_date = `${month}/${day}/${year}`;
    return format_date;
  }

  async get_time(date) {
    let minute = date.getMinutes();
    let hour = date.getHours();
    let ampm = hour >= 12 ? "PM" : "AM";

    if (minute < 10) {
      minute = "0" + minute;
    }
    if (hour < 10) {
      hour = "0" + hour;
    }

    let format_time = `${hour}:${minute} ${ampm}`;
    return format_time;
  }
}
module.exports = new Common();

// search_salon(body, headers) {
//   return new Promise(async (resolve, reject) => {
//     try {
//       let {
//         user_id, search_type, address, salon_or_barber_name, salon_type, service_gender, min_price, max_price
//       } = body;
//       let array = [];

//       if(search_type == 1) {
//         throw {code:400, message:'Unavailable'};
//       }

//       let barberData = await Salon_staff.findAll({
//         attributes: ['salon_id', 'name'],
//       });

//       let salonName = await User.findAll({
//         attributes: ['_id', 'salon_name'],
//         where: {user_type: 2}
//       });

//       for(let i in barberData) {
//         if( barberData[i].name.toLowerCase().includes(salon_or_barber_name.toLowerCase())) {
//           console.log('salon_id ==>>', barberData[i].salon_id);
//           if(!array.includes(barberData[i].salon_id)) {
//             array.push(barberData[i].salon_id);
//           }
//         }
//       }
//       for(let j in salonName) {
//         if( salonName[j].salon_name.toLowerCase().includes(salon_or_barber_name.toLowerCase())) {
//           console.log('_id ==>>', salonName[j]._id);
//           if(!array.includes(salonName[j]._id)) {
//             array.push(salonName[j]._id);
//           }
//         }
//       }
//       console.log('array ==>>', array);

//       const salon_details = await Promise.all(array.map(async (_id) => {
//         return await User.findOne({
//           attributes: [
//             '_id', 'name', 'email', 'country_code', 'mobile', 'salon_type',
//             'profile_pic', 'salon_name', 'address', 'service_gender',
//           ],
//           where: {
//             [Op.and] : [{_id: _id}, {salon_type: salon_type}, {
//               [Op.or] : [{service_gender: service_gender}, {service_gender: 3}]
//             }]
//           }
//         });
//       }));
//       let filtered = salon_details.filter(Boolean);
//       console.log('salon_details ==>>', filtered);

//       return resolve({
//         userData: filtered
//       })
//     }
//     catch(error) {
//       console.log('search_salon service catch error ===>>', error.message);
//       return reject(error);
//     }
//   });
// };



// let { user_id, id } = body;

// let salon = await SalonUser.findAll({
//   include: [
//     {
//       model: UserFavouriteSalons,
//       as: "favsalon",
//       attributes: ["id", "is_favourite"],
//       where: {
//         users_id: `${user_id}`,
//       },
//     },
//   ],
//   where: {
//     id: `${id}`,
//   },
// });

// console.log();
// let all_appointments_of_salon = await OrderOfUser.findAndCountAll({
//   where: {
//     salon_id: `${id}`,
//   },
// });

// let total_app_users = await User.findAndCountAll({});

// return resolve({
//   total_appointments: all_appointments_of_salon.count,
//   total_app_users: total_app_users.count,
// });