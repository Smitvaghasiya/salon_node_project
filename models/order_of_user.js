'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderOfUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderOfUser.belongsTo(models.SalonUser,{
        as:"salon",
        foreignKey:'salon_id',
       
      });
      OrderOfUser.belongsTo(models.User,{
        as:"user",
        foreignKey:'users_id',
       
      });
      OrderOfUser.belongsTo(models.SalonStaff,{
        as:"barber",
        foreignKey:'barber_id',
       
      });
     
      OrderOfUser.belongsToMany(models.Service, 
        {
          through: 'OrderService',
          as: "service",
          foreignKey: 'order_of_users_id'
        });
      
    }
  }
  OrderOfUser.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    salon_id: {
      type: DataTypes.INTEGER,

      name: "FK_salon",
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: true,
    },
    is_cancel: {
      type: DataTypes.SMALLINT,
      allowNull: true,
    },
    
    total: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    users_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: true,
    },
    barber_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "salon_staff",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: true,
    },
    order_date: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    order_time: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    order_status:{
      type: DataTypes.INTEGER,
      defaultValue:'0',
      comment:"0:upcoming,1:completed,2:cancelled",
      allowNull: true
    },
    cancelled_by:{
      type: DataTypes.SMALLINT,
      comment:"1:user,2:salon",
      allowNull: true
    },
    is_walkin_user_appointment:{
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    walkin_user_name:{
      type: DataTypes.STRING,
      allowNull: true
    },
    walkin_user_country_code:{
      type: DataTypes.STRING,
      allowNull: true
    },
    walkin_user_number:{
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'OrderOfUser',
    tableName: 'order_of_users'
  });
  return OrderOfUser;
};