'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SalonStaff extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // define association here
      SalonStaff.belongsTo(models.SalonUser,{
        as:'salon',
        foreignKey:'salon_id'
      })
    }
  }
  SalonStaff.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    salon_id: {
      type: DataTypes.INTEGER,
      
        name: 'FK_salon',
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        
        allowNull: true
    },
    name: {
      type: DataTypes.STRING
    },
    available: {
      type: DataTypes.SMALLINT,
      defaultValue:'1',
      comment: "0,1",
      allowNull: false
    },
    is_deleted: {
      type: DataTypes.SMALLINT,
      defaultValue:'0',
      comment: "0,1",
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'SalonStaff',
    tableName: 'salon_staff'
  });
  return SalonStaff;
};