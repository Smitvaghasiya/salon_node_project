'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PushDevice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PushDevice.init({
    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    user_id:{
      type: DataTypes.INTEGER,

      name: "FK_users_id",
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: false,
    },
    device_token:{
      type:DataTypes.STRING,
      allowNull: true
    },
    device_id:{
      type:DataTypes.STRING,
      allowNull: true
    },
    device_type:{
      type:DataTypes.STRING,
      allowNull: true,
      comment:"1:android,2:ios"
    }

  }, {
    sequelize,
    modelName: 'PushDevice',
    tableName: 'push_device'
  });
  return PushDevice;
};