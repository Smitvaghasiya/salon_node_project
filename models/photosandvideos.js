'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PhotosAndVideos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PhotosAndVideos.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    salon_id: {
      type: DataTypes.INTEGER,
      
        name: 'FK_salon',
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        
        allowNull: true
    },
    images_videos: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    is_image_video:{
      type:DataTypes.SMALLINT,
      allowNull: true,
      comment: '1:image,2:video'
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'PhotosAndVideos',
    tableName: 'photosandvideos'
  });
  return PhotosAndVideos;
};