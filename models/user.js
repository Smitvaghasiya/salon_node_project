'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models){
      User.belongsToMany(models.SalonUser , {
        through: 'WriteReview',
        as: "user",
        foreignKey: 'users_id'
      });
      User.hasMany(models.UserFavouriteSalons, {
        
        as: "fav_salon",
        foreignKey: "users_id",
      });
    }
  }
  User.init({
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      user_type: {
        type: DataTypes.SMALLINT,
        allowNull: true,
        defaultValue:'2',
        comment: "1:user,2:salon"
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true
      },
      country_code: {
        type: DataTypes.STRING,
        allowNull: true
      },
      mobile: {
        type: DataTypes.STRING,
        allowNull: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: true
      },
      gender: {
        type: DataTypes.SMALLINT,
        allowNull: true
      },
      profile_pic: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      address: {
        type: DataTypes.STRING,
        allowNull: true
      },
      is_active: {
        type: DataTypes.SMALLINT,
        allowNull: true
      },
      is_deleted: {
        type: DataTypes.SMALLINT,
        allowNull: true
      },
      is_approved: {
        type: DataTypes.SMALLINT,
        allowNull: true
      },
      otp: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      is_verified: {
        type: DataTypes.SMALLINT,
        allowNull: true
      },
      ratings:{
        type: DataTypes.INTEGER,
        allowNull: true
      },
      referral_code:{
        type: DataTypes.STRING,
        allowNull: true
      },
      latitude:{
        type: DataTypes.DOUBLE,
        allowNull:true
      },
      longitude:{
        type:DataTypes.DOUBLE,
        allowNull:true
      },
      status:{
        type:DataTypes.BOOLEAN,
        allowNull:true
      },
      createdAt: {
        allowNull: true,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: true,
        type: DataTypes.DATE
      }
    }
  , {
    sequelize,
    modelName: 'User',
    tableName: 'users'
  });
  User.addScope('distance', (latitude, longitude, distance, unit = 'km') => {
    const constant = unit == 'km' ? 6371 : 3959;
    const haversine = `(
        ${constant} * acos(
            cos(radians(${latitude}))
            * cos(radians(latitude))
            * cos(radians(longitude) - radians(${longitude}))
            + sin(radians(${latitude})) * sin(radians(latitude))
        )
    )`;
    return {
        attributes: [ 
            [sequelize.literal(haversine), 'distance'],
        ],
        where: sequelize.where(sequelize.literal(haversine), '<=', distance)
    }
}) 
  return User;
};