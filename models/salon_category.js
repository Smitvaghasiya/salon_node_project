'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SalonCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models){
      SalonCategory.belongsToMany(models.SalonUser, {
        through: 'Services',
        as: "user",
        foreignKey: 'category_id'
      });
    }
     
      // define association here

    
  }
  SalonCategory.init({
    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,

    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_active: {
      type: DataTypes.SMALLINT,
      allowNull: true,
    
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
     
    },
    updatedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'SalonCategory',
    tableName: 'salon_category'
  });
  return SalonCategory;
};