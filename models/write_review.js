'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WriteReview extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      WriteReview.belongsTo(models.User, {
        
        
        foreignKey: "users_id",
      });
    }
  }
  WriteReview.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    salon_id:{
      type: DataTypes.INTEGER,
      
        name: 'FK_salon',
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
    },
    users_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: false,
    },
    price: {
      type: DataTypes.INTEGER,
      comment:'1:1,2:2,3:3,4:4,5:5'
    },
    value:{
      type: DataTypes.INTEGER,
      comment:'1:1,2:2,3:3,4:4,5:5'
    },
    quality:{
      type: DataTypes.INTEGER,
      comment:'1:1,2:2,3:3,4:4,5:5'
    },
    friendliness:{
      type: DataTypes.INTEGER,
      comment:'1:1,2:2,3:3,4:4,5:5'
    },
    cleanliness:{
      type: DataTypes.INTEGER,
      comment:'1:1,2:2,3:3,4:4,5:5'
    },
    review_description:{
      type: DataTypes.STRING,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
  sequelize,
    modelName: 'WriteReview',
    tableName: 'write_reviews'
  });
  return WriteReview;
};


