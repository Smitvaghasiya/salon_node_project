'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Service extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      Service.belongsTo(models.SalonUser,
        {
            as: "salon",
            foreignKey:'salon_id'
        });
      Service.belongsToMany(models.OrderOfUser, 
        {
          through: 'OrderService',
          as: "service",
          foreignKey: 'salon_service_id'
        });
       

    }
  }
  Service.init( {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    
    name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    time: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    category_id: {
      type: DataTypes.INTEGER,

      references: {
        model: "salon_category",
        key: "_id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",

      allowNull: true,
    },salon_id: {
        type: DataTypes.INTEGER,
  
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
  
        allowNull: true,
      },
     
   
    
  }, {
    sequelize,
    modelName: 'Service',
    tableName:'salon_service'
  });
  return Service;
};