"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class OrderService extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderService.belongsTo(models.Service,{
       foreignKey:'salon_service_id',
       
      })
    }
  }
  OrderService.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },

      order_of_users_id: {
        type: DataTypes.INTEGER,

        name: "FK_order_of_users",
        references: {
          model: "order_of_users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
        allowNull: false,
      },
      salon_service_id: {
        type: DataTypes.INTEGER,

        name: "FK_salon_service",
        references: {
          model: "salon_service",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
        allowNull: false,
      },
      users_id:{
        type: DataTypes.INTEGER,

        name: "FK_users_id",
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "OrderService",
      tableName: "order_service",
    }
  );
  return OrderService;
};
