'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SalonBusinessHours extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SalonBusinessHours.belongsTo(models.SalonUser,{
        foreignKey:'salon_id'
      })
     
    }
  }
  SalonBusinessHours.init({


    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    salon_id:{
      type: DataTypes.INTEGER,
      
        name: 'FK_salon',
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
    },
    day:{
      type:  DataTypes.INTEGER,
      allowNull: true
    },
    working_start_time:{ 
      type: DataTypes.STRING,
      allowNull: true
    },
    working_end_time:{
      type: DataTypes.STRING,
      allowNull: true
    },
    break_start_time:{
      type: DataTypes.STRING,
      allowNull: true
    },
    break_end_time:{
      type: DataTypes.STRING,
      allowNull: true
    },
    available:{
      type: DataTypes.SMALLINT,
      allowNull: true,
      defaultValue: '0',
      comment: "0:Barber available,1:Barber not available"
    },
    schedule_type:{
      type: DataTypes.SMALLINT,
      allowNull: true,
      defaultValue: '1',
      comment: "1:Weekly_schedule,2:dailySchedule"
    },
    date:{
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt:{
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
    
  }, {
    sequelize,
    modelName: "SalonBusinessHours",
    tableName:"salon_business_hours",
  });
  return SalonBusinessHours;
};