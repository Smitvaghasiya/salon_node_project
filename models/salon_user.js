"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalonUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // static associate(models) {
    //   SalonUser.hasOne(models.SalonStaff,{
    //     foreignKey:'salon_id'
    //   })
    //   // define association here
    // }
    // static associate(models) {
    //   SalonUser.hasMany(models.SalonStaff,{
    //     foreignKey:'salon_id'
    //   })
    //   // define association here
    // }
    static associate(models) {
      // SalonUser.belongsToMany(models.SalonCategory, {
      //   through: "SalonService",
      //   as: "category",
      //   foreignKey: "salon_id",
      // });
      // SalonUser.hasOne(models.SalonStaff,{
      //   as:'salon',
      //   foreignKey:'salon_id'
      // })
      // SalonUser.belongsTo(models.SalonService,{
      //   as:'salon',
      //   foreignKey:'salon_id'
      // })
      SalonUser.belongsToMany(models.User, {
        through: "WriteReview",
        as: "user",
        foreignKey: "salon_id",
      });
      
      SalonUser.hasMany(models.SalonBusinessHours,{
        as:"opening_hours",
        foreignKey:'salon_id'
      });
      // SalonUser.belongsTo(models.Service,{
      //  as:"user",
      //   foreignKey:'salon_id'
      // });
       SalonUser.hasMany(models.UserFavouriteSalons,{
       as:"favsalon",
        foreignKey:'salon_id'
      });
   


      

    }
  }
  SalonUser.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      user_type: {
        type: DataTypes.SMALLINT,
        allowNull: true,
        defaultValue:'2',
        comment: "1:user,2:salon"
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      country_code: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      mobile: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      gender: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      salon_type: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      profile_pic: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      salon_name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      address: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      service_gender: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      is_active: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      is_deleted: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      is_approved: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      otp: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      is_verified: {
        type: DataTypes.SMALLINT,
        allowNull: true,
      },
      details:{
        type: DataTypes.STRING,
        allowNull: true,
      },
      ratings:{
        type: DataTypes.NUMBER,
        allowNull: true
      },
      counts:{
        type: DataTypes.STRING,
        allowNull: true
      },
      referral_code:{
        type: DataTypes.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "SalonUser",
      tableName: "users",
    }
  );
  return SalonUser;
};

// User.belongsToMany(models.Book, {
//   through: 'library',
//   as: 'books',
//   foreignKey: 'user_id'
// })

// const user = await User.findAll({
//   include: [{
//     model: Book,
//     as: 'books',
//     through: {
//       attributes: ['id', 'user_id', 'book_id']
//     },
//     attributes: ['id', 'name']
//   }],
//   attributes: ['id', 'firstName', 'lastName', 'email']
// });
