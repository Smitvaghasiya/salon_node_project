"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalonServiceTest extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SalonServiceTest.belongsTo(models.SalonUser, {
        as: "salon",
        foreignKey: "salon_id",
      });
    }
  }

  SalonServiceTest.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      salon_id: {
        type: DataTypes.INTEGER,

        name: "FK_salon",
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",

        allowNull: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      time: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      category_id: {
        type: DataTypes.INTEGER,
        name:"FK_category",
        references: {
          model: "salon_category",
          key: "_id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",

        allowNull: true,
      },
      is_deleted: {
        type: DataTypes.SMALLINT,
        defaultValue: "0",
        comment: "0,1",
      },
      createdAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: DataTypes.DATE,
      }
    },
    {
      sequelize,
      modelName: "SalonServiceTest",
      tableName: "salon_service_test",
    }
  );
  return SalonServiceTest;
};
