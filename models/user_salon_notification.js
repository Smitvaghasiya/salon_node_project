'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserSalonNotification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserSalonNotification.init({
    
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    send_id: {
      type: DataTypes.INTEGER,

      name: "FK_salon",
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",

      allowNull: false,
    },
    accept_id: {
      type: DataTypes.INTEGER,

      name: "FK_users_id",
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: false,
    },
    title: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    body: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'UserSalonNotification',
    tableName: 'user_salon_notification'
  });
  return UserSalonNotification;
};