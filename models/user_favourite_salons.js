'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserFavouriteSalons extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // define association here
      UserFavouriteSalons.belongsTo(models.SalonUser,{
      
        foreignKey:'salon_id'
      });
   
    }
  }
  UserFavouriteSalons.init({

    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    salon_id:{
      type: DataTypes.INTEGER,
      
        name: 'FK_salon',
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
    },
    users_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "users",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
      allowNull: false,
    },
    is_favourite: {
      type: DataTypes.SMALLINT
    },
    is_deleted: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
   
  }, {
    sequelize,
    modelName: 'UserFavouriteSalons',
    tableName: 'user_favourite_salons'
  });
  return UserFavouriteSalons;
};