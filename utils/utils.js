/**
 * Removes extension from file
 * @param {string} file - filename
 */
exports.removeExtensionFromFile = (file) => {
    return file.split('.').slice(0, -1).join('.').toString();
  };
  
  /**
   * Builds success object
   * @param {string} message - success text
   */
  exports.buildSuccessObject = (message) => {
    return {
      msg: message
    };
  };
  
  /**
   * Builds error object
   * @param {number} code - error code
   * @param {string} message - error text
   */
  exports.buildErrObject = (code, message) => {
    return {
      code,
      message
    };
  };
  
  /**
   * Item not found
   * @param {Object} err - error object
   * @param {Object} item - item result object
   * @param {Object} reject - reject object
   * @param {string} message - message
   */
  exports.itemNotFound = (err, item, reject, message) => {
    if (err) {
      reject(this.buildErrObject(422, err.message));
    }
    if (!item) {
      reject(this.buildErrObject(404, message));
    }
  }