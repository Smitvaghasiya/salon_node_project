const jwt = require('jsonwebtoken');
const semver = require('semver');

const config = require('../config');
const db = require('../config/pgsql');
const response = require('./response');

class HeaderValidator {
  validateHeaders(headers) {
    let error;

    if (!headers.language) {
      error = {
        param: 'language',
        type: 'required'
      };
    } else if (!headers.auth_token) {
      error = {
        param: 'auth_token',
        type: 'required'
      };
    }
    /* else if (!headers.web_app_version) {
       } */
    if (!headers.device_id) {
      error = {
        param: 'device_id',
        type: 'required'
      };
    } else if (!headers.device_type) {
      error = {
        param: 'device_type',
        type: 'required'
      }; //'0' for IOS ,'1' for android
    } else if (!headers.app_version) {
      error = 'APP_VERSION_MISSING';
    }
    /* else if (!headers.os) {
         error = {
           param: 'os',
           type: 'required'
         };
       } */
    else {
      let version = headers.app_version,
        currentAppVersion = config.appVersion,
        tmp_version = version.split('.');

      tmp_version = tmp_version.length < 3 ? tmp_version.concat(['0', '0', '0']) : tmp_version;
      tmp_version.splice(3);
      version = tmp_version.join('.');

      if (semver.valid(version) === null) {
        error = 'INVALID_APP_VERSION';
      } else {
        if (semver.satisfies(version, `>= ${currentAppVersion}`)) { } else {
          error = 'UPGRADE_APP';
        }
      }
    }
    return error;
  }

  nonAuthValidation(req, res, next) {
    let error = module.exports.validateHeaders(req.headers);

    if (error) {
      response.error(res, error, req.headers.language);
    } else if (req.headers.auth_token !== config.defaultAuthToken) {
      response.error(res, 'INVALID_TOKEN', req.headers.language);
    } else {
      // console.log(`nonAuthValidation req.body ->> `, req.body);
      next();
    }
  }

  authValidation(req, res, next) {
    let error = module.exports.validateHeaders(req.headers);

    if (error) {
      response.error(res, 'INVALID_REQUEST_HEADERS', req.headers.language);
    } else {
      jwt.verify(req.headers.auth_token, config.jwtSecretKey, (error, decoded) => {
        // console.log(`\nAuthValidation error ->> ${error} decoded ->> ${JSON.stringify(decoded)}`);

        if (error) {
          if (error.name === 'TokenExpiredError' && req.skip) {
            let decoded = jwt.decode(req.headers.auth_token);

            console.log(`\nAuthValidation decoded ->> ${decoded} token ->> ${req.headers.auth_token}`);
            req.body.user_id = decoded.user_id;

            module.exports.isUserActive(req, res, next);
            // next();
          } else {
            if (req.route.path === '/refreshToken') {
              next();
            } else {
              response.error(res, 'TOKEN_EXPIRED', req.headers.language);
            }
          }
        } else if (decoded && decoded.user_id) {
          req.body.user_id = decoded.user_id;

          module.exports.isUserActive(req, res, next);
          // next();
        } else {
          console.log(`auth validator user_id -->>${decoded.user_id}`);

          response.error(res, 'TOKEN_MALFORMED', req.headers.language);
        }
      });
    }
  }

  skipExpiredToken(req, res, next) {
    req.skip = true;
    next();
  }

  async isUserActive(req, res, next) {
    let selectParams = 'is_active',
      where = `_id = ${req.body.user_id}`,
      userDetail;

    userDetail = await db.select('users', selectParams, where);

    if (!userDetail.length)
      return response.error(
        res,
        'USER_NOT_FOUND',
        req.headers.language,
        401
      );

    if (!userDetail[0].is_active)
      return response.error(
        res,
        'USER_BLOCKED',
        req.headers.language,
        304
      );

    next();
  }
}

module.exports = new HeaderValidator();