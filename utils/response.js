const messages = require('./messages.json');

class Response {
  async error(res, msg,  statusCode = 400) {

    let response = {
      code: 0,
      status: 'FAIL',
      message: messages['en'][msg]||msg,
    }
    res.status(statusCode).json(response);
  }

  async success(res, msg,lan,data, statusCode = 200) {
    let response = {
      code: 1,
      status: 'SUCCESS',
      message: messages['en'][msg]||msg,
      data: data ? data : {}

    }
    console.log(statusCode)

    res.status(statusCode).json(response);
  }

  getMessage(msg, language) {
    let lang = language ? language : 'en';
    if (msg.param && msg.param.includes('email')) {
      msg.param = 'email'
    }
    if (msg.type && msg.type.includes('and')) {
      return msg.message
    }

    if (msg.param && msg.type) {
      if (msg.type.includes('required')) {
        return messages[lang]['PARAMETERS_REQUIRED'].replace('@Parameter@', msg.param)
      } else if (msg.type.includes('min')) {
        return msg.message
      } else {
        return messages[lang]['INVALID_REQUEST_DATA'].replace('@Parameter@', msg.param)
      }
    } else if (msg.toString().includes('ReferenceError:')) {
      return messages[lang]['SOMETHING_WENT_WRONG'];
    } else {
      return messages[lang][msg] || messages[lang]['SOMETHING_WENT_WRONG'];
    }
  }
}

module.exports = new Response()