'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
        return Promise.all([
          queryInterface.addColumn('salon_service', 'salon_id',
            {
              type: Sequelize.INTEGER,
              name: 'FK_salon',
              references: {
                model: 'users',
                key: 'id',
              },
              onUpdate: 'CASCADE',
              onDelete: 'CASCADE',
              
            }),
            queryInterface.changeColumn('salon_service', 'category_id',
            {
              type: Sequelize.UUID,
              name: 'FK_category',
              references: {
                model: 'salon_category',
                key: '_id',
              },
              onUpdate: 'CASCADE',
              onDelete: 'CASCADE',
              
            })
        ]);
      },

      down: (queryInterface, Sequelize) => {
        return Promise.all([
          queryInterface.removeColumn('salon_service', 'salon_id'),
        ]);
      }
    };

