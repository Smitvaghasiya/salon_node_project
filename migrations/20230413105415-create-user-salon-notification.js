"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("user_salon_notification", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      salon_id: {
        type: Sequelize.INTEGER,

        name: "FK_salon",
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",

        allowNull: false,
      },
      users_id: {
        type: Sequelize.INTEGER,

        name: "FK_users_id",
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
        allowNull: false,
      },
      title: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      body: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      notification_by_user_salon: {
        type: Sequelize.INTEGER,

        
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",

        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("user_salon_notification");
  },
};
