'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_type: {
        allowNull: true,
        defaultValue: '1',
        type: Sequelize.STRING,
        comment:"1:salon,2:user"
      },
      name: {
        allowNull: true,
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING,
        unique: true
      },
      salon_type: {
        defaultValue: '1',
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"1:hair,2:nail"
      },
      profile_pic: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      salon_name: {
        allowNull: true,
        type: Sequelize.STRING
      },
      gender: {
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"1:male,2:female"
      },
      address: {
        allowNull: true,
        type: Sequelize.STRING
      },
      service_gender: {
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"1:male,2:female,3:both"
      },
      country_code: {
        allowNull: true,
        type: Sequelize.STRING
      },
      mobile: {
        allowNull: true,
        type: Sequelize.STRING,
        unique: true
      },
      password: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      is_active: {
        defaultValue: '1',
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"0,1"
      },
      is_approved: {
        defaultValue: '0',
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"0,1"
      },
      is_deleted: {
        defaultValue: '0',
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"0,1"
      },
      otp: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      is_verified: {
        defaultValue: '0',
        allowNull: true,
        type: Sequelize.SMALLINT,
        comment:"0,1"
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('users');
  }
};