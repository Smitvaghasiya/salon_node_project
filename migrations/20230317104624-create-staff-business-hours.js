'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('staff_business_hours', {
      _id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      salon_id:{
        type: Sequelize.INTEGER,
        
          name: 'FK_salon',
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
          allowNull: false
      },
      barber_id:{
        type: Sequelize.INTEGER,
        
        name: 'FK_barber',
        references: {
          model: 'salon_staff',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
      day:{
        type:  Sequelize.INTEGER,
        allowNull: true
      },
      working_start_time:{ 
        type: Sequelize.STRING,
        allowNull: true
      },
      working_end_time:{
        type: Sequelize.STRING,
        allowNull: true
      },
      break_start_time:{
        type: Sequelize.STRING,
        allowNull: true
      },
      break_end_time:{
        type: Sequelize.STRING,
        allowNull: true
      },
      available:{
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: '0',
        comment: "0:Barber available,1:Barber not available"
      },
      schedule_type:{
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: '1',
        comment: "1:Weekly_schedule,2:dailySchedule"
      },
      createdAt:{
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('staff_business_hours');
  }
};