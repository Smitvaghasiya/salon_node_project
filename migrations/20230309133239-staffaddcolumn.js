'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
        return Promise.all([
          queryInterface.changeColumn('salon_staff', 'salon_id',
            {
              type: Sequelize.INTEGER,
              name: 'FK_salon',
              references: {
                model: 'users',
                key: 'id',
              },
              onUpdate: 'CASCADE',
              onDelete: 'CASCADE',
              
            }),
           
        ]);
      },

      down: (queryInterface, Sequelize) => {
        return Promise.all([
          queryInterface.removeColumn('salon_staff', 'salon_id'),
        ]);
      }
    };
