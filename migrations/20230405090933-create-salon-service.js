'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('salon_service', {
      
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        salon_id: {
          type: Sequelize.INTEGER,
  
          name: "FK_salon",
          references: {
            model: "users",
            key: "id",
          },
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
  
          allowNull: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        price: {
          type: Sequelize.FLOAT,
          allowNull: true,
        },
        time: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        description: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        category_id: {
          type: Sequelize.INTEGER,
  
          references: {
            model: "salon_category",
            key: "_id",
          },
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
  
          allowNull: true,
        },
        is_deleted: {
          type: Sequelize.SMALLINT,
          defaultValue: "0",
          comment: "0,1",
        },
        created_at: {
          allowNull: true,
          type: Sequelize.DATE,
        },
        updated_at: {
          allowNull: true,
          type: Sequelize.DATE,
        }
      
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('salon_service');
  }
};