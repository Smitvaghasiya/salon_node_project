'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('write_reviews', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      salon_id:{
        type: Sequelize.INTEGER,
        
          name: 'FK_salon',
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
          allowNull: false
      },
      users_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
        allowNull: false,
      },
      price: {
        type: Sequelize.INTEGER,
        comment:'1:1,2:2,3:3,4:4,5:5'
      },
      value:{
        type: Sequelize.INTEGER,
        comment:'1:1,2:2,3:3,4:4,5:5'
      },
      quality:{
        type: Sequelize.INTEGER,
        comment:'1:1,2:2,3:3,4:4,5:5'
      },
      friendliness:{
        type: Sequelize.INTEGER,
        comment:'1:1,2:2,3:3,4:4,5:5'
      },
      cleanliness:{
        type: Sequelize.INTEGER,
        comment:'1:1,2:2,3:3,4:4,5:5'
      },
      review_description:{
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('write_reviews');
  }
};