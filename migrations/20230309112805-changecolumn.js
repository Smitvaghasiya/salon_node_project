'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('users', 'email', 
        {
          type: Sequelize.STRING,
          unique: true
        }
        
    ),
    ]);
  },

  down: (queryInterface) => {
    return Promise.all([queryInterface.changeColumn('users')]);
  },
};
