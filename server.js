require("express-group-routes");
const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const app = express();
const path = require("path");
const initPgsql = require("./config/db");
const db = require("./config/db");
app.set("port", process.env.PORT || 3000);
const { QueryTypes } = require("sequelize");
const cron = require("node-cron");
const { get_date, get_time, convertTimeFormat } = require("./common/common");
const { OrderOfUser, UserSalonNotification, PushDevice } = require("./models");
const firebase = require("./config/firebase");
const moment = require("moment-timezone");

app.use(
  bodyParser.json({
    limit: "50mb",
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
  })
);

app.use(express.static(path.join(__dirname, "public")));

app.use("/images", express.static(path.join(__dirname, "images")));

app.use("/image", express.static(path.join(__dirname, "image")));
app.set("views", path.join(__dirname, "views"));

module.exports = app;

// cron.schedule("* * * * * *", async function () {
//   let order = await OrderOfUser.findAll({});
//   let for_date = await get_date(new Date());
//   let for_time = await get_time(new Date());
//   let convert_time_format = await convertTimeFormat(for_time);
//   let format_date = await moment(`${for_date}`).format("MM/DD/YYYY");

  

//   console.log("convert_time_format", convert_time_format);
//   console.log("format_date", format_date);

//   let compare_date = order.filter((a) => {
//     let date = moment(a.order_date).format("MM/DD/YYYY");
//     let T = convertTimeFormat(a.order_time);

//     console.log("date", moment(a.order_date).format("MM/DD/YYYY"));
//     console.log(new Date(date).getTime() <= new Date(format_date).getTime());

//     return new Date(date).getTime() <= new Date(format_date).getTime();
//   });
//   console.log("final", compare_date[1].dataValues.id);

//   for (let i in order) {
//     if (
//       order[i].dataValues.order_date == format_date &&
//       (convertTimeFormat(order[i].dataValues.order_time) ==
//         convert_time_format ||
//         convertTimeFormat(order[i].dataValues.order_time) <=
//           convert_time_format)
//     ) {
//       await OrderOfUser.update(
//         {
//           order_status: 1,
//         },
//         {
//           where: {
//             is_cancel: 0,
//             id: `${order[i].dataValues.id}`,
//           },
//         }
//       );
//       console.log("order_completed");

//       let token = await PushDevice.findAll({
//         attributes: ["device_type", "device_token"],
//         where: {
//           user_id: `${order[i].dataValues.users_id}`,
//         },
//       });
//       console.log(token[0].device_token);

//       if (token.length > 0) {
//         let title = "Salon Booking",
//           body = "appointment completed successfully";
//         let notification = {
//           title,
//           body,
//         };

//         console.log(token[0].device_token);

//         token = token[0].device_token;

//         await firebase.sendFCMPushNotification(token, notification, {
//           name: "test",
//         });

//         let Data = {
//           accept_id: `${order[i].dataValues.id}`,
//           title: `${title}`,
//           body: `${body}`,
//         };

//         let Notification = await UserSalonNotification.create(Data);
//       }
//     }
//   }

//   // for (let i in compare_date) {
//   //   console.log(i);

//   //   await OrderOfUser.update(
//   //     {
//   //       order_status: 1,
//   //     },
//   //     {
//   //       where: {
//   //         is_cancel: 0,
//   //         id: `${compare_date[i].dataValues.id}`,
//   //       },
//   //     }
//   //   );
//   //   console.log("order_completed");

//   //   let token = await PushDevice.findAll({
//   //     attributes: ["device_type", "device_token"],
//   //     where: {
//   //       user_id: `${compare_date[i].dataValues.users_id}`,
//   //     },
//   //   });
//   //   console.log(token[0].device_token);
//   //   console.log("final", compare_date[i].dataValues.id);

//   //   var title = "Salon Booking",
//   //     body = "appointment completed successfully";
//   //   let notification = {
//   //     title,
//   //     body,
//   //   };

//   //   console.log(token[0].device_token);

//   //   token = token[0].device_token;

//   //   await firebase.sendFCMPushNotification(token, notification, {
//   //     name: "test",
//   //   });

//   //   UserSalonNotification.create({
//   //     send_id: `${compare_date[i].dataValues.salon_id}`,
//   //     accept_id: `${compare_date[i].dataValues.users_id}`,
//   //     title: `${title}`,
//   //     body: `${body}`,
//   //   });
//   // }
// });

require("./app/routes/index").routerConfig(app);

Promise.all([require("./config/httpServer")()])
  .then((values) => {
    server.listen(app.get("port"), () => {
      console.log(
        ` ----------------------- Server listening on the port ${app.get(
          "port"
        )} ${new Date()} ----------------------- `
      );

      initPgsql.getDBConnect().then(() => {
        console.log(`Pgsql Connected`);
      });
    });
  })
  .catch((error) => {
    console.log(
      `----------------------- Main server configuration error >> ${error} \n---------------------------------------------- `
    );
  });
