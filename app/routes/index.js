const path = require("path");

const userController = require("../controllers/userController");
const multer = require("multer");
const userValidation = require("../middleware/userValidation");

//image_videos_upload
const imagestorage = multer.diskStorage({
  // Destination to store image
  destination: "image",
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
    // file.fieldname is name of the field (image)
    // path.extname get the uploaded file extension
  },
});

const imageupload = multer({
  storage: imagestorage,
  limits: {
    fileSize: 10000000, // 1000000 Bytes = 1 MB
  },
  fileFilter(req, file, cb) {
    console.log("========", new Date());
    if (!file.originalname.match(/\.(png|jpg|jpeg|mp4)$/)) {
      // upload only png and jpg format
      return cb(new Error("Please upload a Image"));
    }
    cb(undefined, true);
  },
});

//profile_image
const imageStorage = multer.diskStorage({
  // Destination to store image
  destination: "images",
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
    // file.fieldname is name of the field (image)
    // path.extname get the uploaded file extension
  },
});

const imageUpload = multer({
  storage: imageStorage,
  limits: {
    fileSize: 10000000, // 1000000 Bytes = 1 MB
  },
  fileFilter(req, file, cb) {
    console.log("========", new Date());
    if (!file.originalname.match(/\.(png|jpg|jpeg)$/)) {
      // upload only png and jpg format
      return cb(new Error("Please upload a Image"));
    }
    cb(undefined, true);
  },
});

// edit_img_video

const imgStorage = multer.diskStorage({
  // Destination to store image
  destination: "image",
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
    // file.fieldname is name of the field (image)
    // path.extname get the uploaded file extension
  },
});

const editimgupload = multer({
  storage: imgStorage,
  limits: {
    fileSize: 10000000, // 1000000 Bytes = 1 MB
  },
  fileFilter(req, file, cb) {
    console.log("========", new Date());
    if (!file.originalname.match(/\.(png|jpg|jpeg|mp4)$/)) {
      // upload only png and jpg format
      return cb(new Error("Please upload a Image"));
    }
    cb(undefined, true);
  },
});

// const oldPath = path.join(__dirname, "..", "images", oldPhoto);
// fs.unlink(oldPath)

exports.routerConfig = (app) => {
  app.get("/health", (req, res, error) => {
    res.send("success");
  });

  app.post(
    "/salon_register",
    imageUpload.single("profile_pic"),
    userController.salon_register
  );
  app.post("/salon_login", userController.salon_login);
  app.post(
    "/add_barber",
    userValidation.verifyTokenMid,
    userController.add_barber
  );
  app.post(
    "/list_barber",
    userValidation.verifyTokenMid,
    userController.list_barber
  );
  app.post("/hasone", userController.hasone);
  app.post(
    "/add_category",
    userValidation.verifyTokenMid,
    userController.add_category
  );
  app.post(
    "/get_category",
    userValidation.verifyTokenMid,
    userController.get_category
  );
  app.post(
    "/get_service",
    userValidation.verifyTokenMid,
    userController.get_service
  );
  app.post(
    "/edit_service",
    userValidation.verifyTokenMid,
    userController.edit_service
  );
  app.post(
    "/delete_service",
    userValidation.verifyTokenMid,
    userController.delete_service
  );
  app.post(
    "/add_service",
    userValidation.verifyTokenMid,
    userController.add_service
  );
  app.post(
    "/list_service",
    userValidation.verifyTokenMid,
    userController.list_service
  );
  app.post(
    "/user_register",
    imageUpload.single("profile_pic"),
    userController.user_register
  );
  app.post("/user_login", userController.user_login);
  app.post("/forgot_password", userController.forgot_password);
  app.post("/sendotp", userController.sendotp);
  app.post("/verifyotp", userController.verifyotp);
  app.post("/new_password", userController.new_password);
  app.post(
    "/reset_password",
    userValidation.verifyTokenMid,
    userController.reset_password
  );
  app.post("/belongsto", userController.belongsto);
  app.post("/hasmany", userController.hasmany);
  app.post("/belongstomany", userController.belongstomany);
  app.post("/refreshtoken", userController.refreshtoken);
  app.post(
    "/add_businesshours",
    userValidation.verifyTokenMid,
    userController.add_businesshours
  );
  app.post("/edit_businesshours", userController.edit_businesshours);
  app.post(
    "/list_weeklybusinesshour",
    userValidation.verifyTokenMid,
    userController.list_weeklybusinesshour
  );
  app.post(
    "/list_dailybusinesshour",
    userValidation.verifyTokenMid,
    userController.list_dailybusinesshour
  );
  app.post(
    "/weeklydailyschedule",
    userValidation.verifyTokenMid,
    userController.weeklydailyschedule
  );
  app.post(
    "/add_barberbusinesshours",
    userValidation.verifyTokenMid,
    userController.add_barberbusinesshours
  );
  app.post(
    "/list_barberdailyhours",
    userValidation.verifyTokenMid,
    userController.list_barberdailyhours
  );
  app.post(
    "/list_barberweeklyhours",
    userValidation.verifyTokenMid,
    userController.list_barberweeklyhours
  );
  app.post(
    "/edit_userprofile",
    userValidation.verifyTokenMid,
    userController.edit_userprofile
  );
  app.post(
    "/contactus",
    userValidation.verifyTokenMid,
    userController.contactus
  );
  app.post("/search", userValidation.verifyTokenMid, userController.search);
  app.post(
    "/add_writereview",
    userValidation.verifyTokenMid,
    userController.add_writereview
  );
  app.post(
    "/list_review",
    userValidation.verifyTokenMid,
    userController.list_review
  );
  app.post(
    "/add_userfavouritesalon",
    userValidation.verifyTokenMid,
    userController.add_userfavouritesalon
  );
  // app.post('/delete_userfavouritesalon',userValidation.verifyTokenMid,userController.delete_userfavouritesalon)
  app.post(
    "/list_userfavouritesalon",
    userValidation.verifyTokenMid,
    userController.list_userfavouritesalon
  );
  app.post(
    "/add_salondetail",
    userValidation.verifyTokenMid,
    userController.add_salondetail
  );
  app.post(
    "/list_salondetail",
    userValidation.verifyTokenMid,
    userController.list_salondetail
  );
  app.post(
    "/add_imagevideo",
    imageupload.array("images_videos"),
    userValidation.verifyTokenMid,
    userController.add_imagevideo
  );
  app.post(
    "/edit_imagevideo",
    editimgupload.single("images_videos"),
    userValidation.verifyTokenMid,
    userController.edit_imagevideo
  );
  app.post(
    "/delete_imagevideo",
    userValidation.verifyTokenMid,
    userController.delete_imagevideo
  );
  app.post(
    "/list_imagevideo",
    userValidation.verifyTokenMid,
    userController.list_imagevideo
  );
  app.post(
    "/list_salons_detail",
    userValidation.verifyTokenMid,
    userController.list_salons_detail
  );
  app.post("/referal_code", userController.referal_code);
  app.post(
    "/add_order",
    userValidation.verifyTokenMid,
    userController.add_order
  );
  app.post(
    "/order_detail",
    userValidation.verifyTokenMid,
    userController.order_detail
  );
  app.post(
    "/date_time",
    userValidation.verifyTokenMid,
    userController.date_time
  );
  app.post(
    "/order_cancelled_by_user_salon",
    userValidation.verifyTokenMid,
    userController.order_cancelled_by_user_salon
  );
  app.post(
    "/user_appointment",
    userValidation.verifyTokenMid,
    userController.user_appointment
  );
  app.post(
    "/salon_appointment",
    userValidation.verifyTokenMid,
    userController.salon_appointment
  );
  app.post(
    "/user_appointment_completed",
    userValidation.verifyTokenMid,
    userController.user_appointment_completed
  );
  app.post(
    "/user_device_relation",
    userValidation.verifyTokenMid,
    userController.user_device_relation
  );
  app.post(
    "/salon_appointment_calender",
    userValidation.verifyTokenMid,
    userController.salon_appointment_calender
  );
  app.post(
    "/barber_appointment_calender",
    userValidation.verifyTokenMid,
    userController.barber_appointment_calender
  );
  app.post(
    "/add_notification",
    userValidation.verifyTokenMid,
    userController.add_notification
  );
  app.post(
    "/delete_notification",
    userValidation.verifyTokenMid,
    userController.delete_notification
  );
  app.post(
    "/list_notification",
    userValidation.verifyTokenMid,
    userController.list_notification
  );
  app.post(
    "/cron_job",
    userValidation.verifyTokenMid,
   userController.cron_job
  );
  app.post(
    "/salon_earnings",
    userValidation.verifyTokenMid,
   userController.salon_earnings
  );

  app.post(
    "/salon_dashboard",
    userValidation.verifyTokenMid,
   userController.salon_dashboard
  );

  app.post(
    "/user_dashboard",
    userValidation.verifyTokenMid,
   userController.user_dashboard
  );

  app.post(
    "/salon_view_review",
    userValidation.verifyTokenMid,
   userController.salon_view_review
  );

  app.post(
    "/lat_longtitude",
    userValidation.verifyTokenMid,
   userController.lat_longtitude
  );


  app.post("/send_notification", userController.send_notification);
};
