const joi=require('joi')

const editcategoryschema=joi.object({
    name:joi.string().required(),
    price:joi.number().required(),
    time:joi.string().required(),
    description:joi.string().required(),
    category_id:joi.number().required(),
    Id:joi.number().required()
    
})

module.exports={
    editcategoryschema
}