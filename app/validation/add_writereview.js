const joi=require('joi')
const promise = require('bluebird')

const joiValidator=require('../middleware/joiValidator')


    const writereviewschema=joi.object({
       
        user_id: joi.number().required() ,
       
        salon_id: joi.number().required(),
        price: joi.number().required(),
        value: joi.number().required(),
        quality: joi.number().required(),
        friendliness: joi.number().required(),
        cleanliness: joi.number().required(),
        review_description :joi.string().required()
    })

    

module.exports={writereviewschema}