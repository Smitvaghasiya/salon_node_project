const joi=require('joi')

const resetpasswordschema=joi.object({
    user_id: joi.number().required(),
    old_password: joi.string().min(2).required(),
    new_password: joi.string().min(2).required(),
  
    
})

module.exports={
    resetpasswordschema
}