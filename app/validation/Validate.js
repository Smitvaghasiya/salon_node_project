const joi = require("joi");
const joiValidator = require("../middleware/joiValidator");
const promise = require("bluebird");

class Validate {
  add_userfavouritesalon = joi.object({
    user_id: joi.number().required(),
    salon_id: joi.number().required(),
    // is_favourite: joi.number().required(),
    is_deleted: joi.number().required(),
  });

  add_salondetail = joi.object({
    details: joi.string().required(),
  });

  listsalonsdetail = joi.object({
    user_id: joi.number().required(),
    id: joi.number().required(),
  });

  edit_imagevideo = joi.object({
    user_id: joi.number().required(),
    images_videos: joi.string().required(),
    id: joi.number().required(),
  });

  deleteimagevideo = joi.object({
    user_id: joi.number().required(),
    id: joi.number().required(),
  });

  add_imagevideo = joi.object({
    user_id: joi.number().required(),
    images_videos: joi.string().required(),
  });

  refercode = joi.object({
    id: joi.number().required(),
  });

  add_order = joi.object({
    salon_service_id: joi.array().items(joi.number()),
    user_id: joi.number().required(),
    total: joi.number().required(),
    salon_id: joi.number().required(),
    barber_id: joi.number().required(),
    order_date: joi.string().required(),
    order_time: joi.string().required(),
  });

  date_time = joi.object({
    id: joi.number().required(),
    date: joi.string().required(),
    user_id: joi.number().required(),
    barber_id: joi.number().required(),
    salon_id: joi.number().required(),
  });

  cancelled_order = joi.object({
    user_id: joi.number().required(),
    id: joi.number().required(),
  });

  appointment = joi.object({
    page: joi.number(),
    size: joi.number(),
    user_id: joi.number().required(),
    order_status: joi.number().required(),
    start_date: joi.string(),
    end_date: joi.string(),
  });

  completed_order = joi.object({
    accept_id: joi.number().required(),
    user_id: joi.number().required(),
    id: joi.number().required(),
  });

  user_device_relation = joi.object({
    user_id: joi.number().required(),
    device_token: joi.string().required(),
    device_type: joi.number().required(),
    device_id: joi.string().required(),
  });

  salon_appointment_calender = joi.object({
    user_id: joi.number().required(),
    order_date: joi.string().required(),
    page: joi.number(),
    size: joi.number(),
  });

  barber_appointment_calender = joi.object({
    user_id: joi.number().required(),
    order_date: joi.string().required(),
    barber_id: joi.number().required(),
    page: joi.number(),
    size: joi.number(),
  });

  delete_notification= joi.object({
    user_id: joi.number().required(),
    id: joi.number().required(),
  });

  salon_earnings=joi.object({
    user_id: joi.number().required(),
    salon_sale: joi.number().required(),
    start_date :joi.string().required(), 
    end_date:joi.string().required(), 
    order_status :joi.number().required(),
    page: joi.number(),
    size: joi.number()
  });

  salon_view_review=joi.object({
    user_id: joi.number().required(),
    id: joi.number().required(),
  });

}

module.exports = new Validate();
