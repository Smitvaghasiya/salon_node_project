const joi=require('joi')
const promise = require('bluebird')

const joiValidator=require('../middleware/joiValidator')


    const weeklyscheduleschema=joi.object({
       
        user_id: joi.number().required() ,
        schedule_type: joi.number().required() ,
        // day: joi.number().greater(0).less(8).required().label('Day'),
        working_start_time: joi.string().required(),
        working_end_time : joi.string().required(),
        break_start_time : joi.string().required(),
        break_end_time: joi.string().required() ,
        
        day: joi.number()
    })

    

module.exports={weeklyscheduleschema}