

const joi=require('joi')
const promise = require('bluebird')

const joiValidator=require('../middleware/joiValidator')


    const addstaffweekbusshour=joi.object({
        day: joi.number(),
        user_id: joi.number().required() ,
        barber_id: joi.number().required(),
        // day: joi.number().greater(0).less(8).required().label('Day'),
        date: joi.string(),
        working_start_time: joi.string().required(),
        working_end_time : joi.string().required(),
        break_start_time : joi.string().required(),
        break_end_time: joi.string().required() ,
        schedule_type:joi.number().required()
    })

    

module.exports={addstaffweekbusshour}