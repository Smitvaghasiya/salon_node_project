const joi=require('joi')

const edituserprofile=joi.object({
    user_id: joi.number().required(),
     name: joi.string().required(), 
     email: joi.string().email().lowercase().required(),
     country_code: joi.string().required(), 
     mobile: joi.string().required(), 
     gender: joi.number().required()
})

module.exports={
    edituserprofile
}