const joi = require("joi");
const promise = require("bluebird");

const joiValidator = require("../middleware/joiValidator");

const searchschema = joi.object({
  service_gender: joi.number().required(),
  // nam: joi.string().required(),
  // id: joi.number().required(),
  user_id: joi.number().required(),
  salon_name: joi.string().required(),
  // name: joi.string().required(),
  salon_type: joi.number().required(),
  priceFrom: joi.number(),
  priceTo: joi.number(),
  latitude: joi.number(),
  longitude: joi.number(),
  distanceFrom: joi.number(),
  distanceTo :joi.number(),
  page: joi.number(),
  size: joi.number(),
});

module.exports = { searchschema };
