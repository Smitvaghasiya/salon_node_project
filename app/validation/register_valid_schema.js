const joi= require('joi')

const authschema=joi.object({

    email: joi.string().email().lowercase().required(),
    password: joi.string().min(8).required().label('password'),
    
    // user_type : joi.number().integer().label("User type"),
    name : joi.string().required().label('name'),
    email : joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).required().label('email'),
    mobile : joi.number().required().label('mobile number'),
    // password : joi.string()
    // .minOfLowercase(2).minOfUppercase(2).minOfNumeric(2).required().label('password'),
                // confirmpassword : joi.ref('password'),
    salon_name : joi.string().required().label('salon_name'),
    // address : joi.string().required().label('address'),
    // salon_type : joi.number().required().label('salon_type'),
    service_gender: joi.number().required(),
    gender:joi.number().required(),
    country_code:joi.string().required(),
    // is_active:joi.number().required(),
    // is_approved:joi.number().required(),
    // is_deleted:joi.number().required(),
    // otp:joi.number().required(),
    // is_verified:joi.number().required(),
    profile_pic:joi.string().required(),
    address:joi.string().required()


})

module.exports={
    authschema
}