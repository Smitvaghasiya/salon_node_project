const joi=require('joi')
const promise = require('bluebird')

const joiValidator=require('../middleware/joiValidator')


    const listdailybusinesshour=joi.object({
       
        user_id: joi.number().required() ,
         day: joi.number().greater(0).less(8).required().label('Day'),
        date: joi.string().required(),
        schedule_type: joi.number().required()
    })

    

module.exports={listdailybusinesshour}