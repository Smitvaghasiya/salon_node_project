const joi=require('joi')
const promise = require('bluebird')

const joiValidator=require('../middleware/joiValidator')


    const salonservicesschema=joi.object({
        user_id: joi.number().required(),
        name:joi.string().required(),
        price:joi.number().required(),
        time:joi.string().required(),
        description:joi.string().required(),
        category_id:joi.number().required()
    })

    

module.exports={salonservicesschema}
