const joi=require('joi')

const loginschema=joi.object({
    email: joi.string().email().lowercase().required(),
    password: joi.string().min(2).required(),
    // id:joi.number().required()
    
})

module.exports={
    loginschema
}