const moment = require("moment-timezone");
const {
  getJwtToken,
  generateOTP,
  profile_pic,
  images_videos,
  print24,
  convertTimeFormat,
  generateCode,
  DateFormat,
  addMinutes,
  AddMinutesToDate,
  OnlyTime,
  convertTime,
  paginate,
  paginatedata,
  get_date,
  get_time,
} = require("../../common/common");
// const db = require("./../../config/db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {
  Service,
  SalonCategory,
  SalonUser,
  SalonStaff,
  SalonService,
  User,
  SalonBusinessHours,
  StaffBusinessHours,
  ContactUs,
  WriteReview,
  UserFavouriteSalons,
  SalonDetail,
  PhotosAndVideos,
  OrderOfUser,
  OrderTotal,
  OrderService,
  PushDevice,
  UserSalonNotification,
} = require("../../models");
const db = require("../../models");
const fs = require("fs");
const nodemailer = require("nodemailer");
const { loginschema } = require("../validation/login_valid_schema");
const { authschema } = require("../validation/register_valid_schema");
const userValidation = require("../middleware/userValidation");
const { addcategoryschema } = require("../validation/addcategory");
const { error } = require("../../utils/response");
const { salonservicesschema } = require("../validation/salonservices");
const { deleteservicesschema } = require("../validation/deleteservices");
const { editcategoryschema } = require("../validation/editcategoryschema");
const { addstaffschema } = require("../validation/addstaff");
const { getserviceschema } = require("../validation/getservice");
const { getcategoryschema } = require("../validation/getcategory");
const { userauthschema } = require("../validation/userregister");
const { userloginschema } = require("../validation/userlogin");
const { newpasswordschema } = require("../validation/newpassword");
const { sendotpschema } = require("../validation/sendotp");
const Sequelize = require("sequelize");
const salon_staff = require("../../models/salon_staff");
const { new_password } = require("../controllers/userController");
const { resetpasswordschema } = require("../validation/resetpassword");
const Op = Sequelize.Op;
const config = require("../../utils/config");
const { refreshtokenschema } = require("../validation/refreshtoken");
const { addbusshourschema } = require("../validation/addbusinesshours");
const { editbusinesshourschema } = require("../validation/editbusinesshours");
const { listbusinesshourschema } = require("../validation/list_businesshour");
const { dailyscheduleschema } = require("../validation/dailyschedule");
const { weeklyscheduleschema } = require("../validation/weeklyschedule");
const { listdailybusinesshour } = require("../validation/listdailybusshour");
const {
  addstaffdailybusshour,
} = require("../validation/addstaffdailybusshour");
const { edituserprofile } = require("../validation/edit_userprofile");
const { addstaffweekbusshour } = require("../validation/addstaffweekbusshour");
const { contactusschema } = require("../validation/contactus");
const { searchschema } = require("../validation/search");
const { writereviewschema } = require("../validation/add_writereview");
// const sequelize = require("sequelize");
const { QueryTypes } = require("sequelize");
const sequelize = require("../../config/dab1");
const {
  add_userfavouritesalon,
  add_salondetail,
  edit_imagevideo,
  listsalonsdetail,
  deleteimagevideo,
  add_imagevideo,
  refercode,
  add_order,
  date_time,
  cancelled_order,
  appointment,
  completed_order,
  user_device_relation,
  salon_appointment_calender,
  barber_appointment_calender,
  delete_notification,
  salon_earnings,
  salon_view_review,
} = require("../validation/Validate.js");
const referCode = require("referral-code-generator");
const { array } = require("joi");
const order_total = require("../../models/order_service");
const internal = require("stream");
const firebase = require("./../../config/firebase");
const cron = require("node-cron");
const NodeGeocoder = require("node-geocoder");
const console = require("console");

class UserService {
  salon_register(body) {
    return new Promise(async (resolve, reject) => {
      try {
        console.log(body);
        let {
          user_type,
          name,
          email,
          profile_pic,
          salon_name,
          service_gender,
          address,
          gender,
          country_code,
          mobile,
          password,
        } = body;

        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        const result = await authschema.validateAsync(body);
        // const result = await authschema.validateAsync(body);
        const user = await SalonUser.findAll({
          where: {
            email: `${email}`,
          },
        });
        if (user.length > 0) {
          console.log("email alredy exist");
          let error = Error("EMAIL_ALREADY_EXISTS");
          error.code = 400;
          return reject(error);
        } else if (user.length == 0) {
          bcrypt
            .hash(`${password}`, 10)
            .then(async (hash) => {
              if (hash) {
                let string = `${email}`;
                let refercode = string.substring(0, 4);
                let referalcode = referCode.custom(
                  "uppercase",
                  3,
                  5,
                  `${refercode}`
                );
                await SalonUser.create({
                  name: `${name}`,
                  email: `${email}`,
                  profile_pic: `${profile_pic}`,
                  gender: `${gender}`,
                  salon_name: `${salon_name}`,
                  service_gender: `${service_gender}`,
                  address: `${address}`,
                  country_code: `${country_code}`,
                  mobile: `${mobile}`,
                  password: `${hash}`,
                  referral_code: `${referalcode}`,
                  // createdAt: `${date}`,
                  // updatedAt: `${date}`,
                });

                const token = generateAccessToken({ user: `${email}` });
                console.log("token:", token);
                function generateAccessToken(user) {
                  return jwt.sign(
                    user,
                    (process.env.jwtSecretKey = "secretCode"),
                    { expiresIn: "15m" }
                  );
                }
                const user_data = await SalonUser.findAll({
                  attributes: [
                    "id",
                    "user_type",
                    "name",
                    "email",
                    "profile_pic",
                    "salon_name",
                    "salon_type",
                    "service_gender",
                    "address",
                    "country_code",
                    "mobile",
                    "is_active",
                    "is_verified",
                    "referral_code",
                    "createdAt",
                    "updatedAt",
                  ],

                  where: {
                    email: `${email}`,
                  },
                });

                return resolve({
                  "token:": token,
                  userdata: {
                    _id: user_data[0].id,
                    user_type: user_data[0].user_type,
                    name: user_data[0].name,
                    email: user_data[0].email,
                    profile_pic: user_data[0].profile_pic,
                    gender: user_data[0].gender,
                    salon_name: user_data[0].salon_name,
                    service_gender: user_data[0].service_gender,
                    address: user_data[0].address,
                    country_code: user_data[0].country_code,
                    mobile: user_data[0].mobile,
                    is_active: user_data[0].is_active,
                    is_verified: user_data[0].is_verified,
                    referral_code: user_data[0].referral_code,
                    createdAt: user_data[0].createdAt,
                    updatedAt: user_data[0].updatedAt,
                  },
                });
              }
            })
            .catch((err) => console.log(err));
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  salon_login(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { email, password } = body;
        const result = await loginschema.validateAsync(body);
        const user = await SalonUser.findAll({
          where: {
            email: `${email}`,
          },
        });

        if (user.length == 0) {
          throw { message: "email is not valid", code: 403 };
        } else {
          let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");

          //query
          let user_data = await SalonUser.findAll({
            attributes: [
              "id",
              "name",
              "email",
              "profile_pic",
              "password",
              "gender",
              "salon_name",
              "service_gender",
              "address",
              "country_code",
              "mobile",
              "otp",
              "is_active",
              "is_verified",
              "createdAt",
              "updatedAt",
            ],

            where: {
              email: `${email}`,
            },
          })
            // console.log("userdata", user_data)
            .then(async (user_data) => {
              //compare password
              if (
                user_data &&
                (await bcrypt.compare(`${password}`, user_data[0].password))
              ) {
                const token = generateAccessToken(`${user_data[0].id}`);

                delete user_data[0].password;
                console.log("userdata", user_data);

                function generateAccessToken(userId) {
                  return jwt.sign(
                    { userId },
                    (process.env.jwtSecretKey = "secretCode"),
                    { expiresIn: "3h" }
                  );
                }
                let profile = profile_pic(user_data[0].profile_pic);
                return resolve({
                  token: token,
                  userdata: {
                    _id: user_data[0].id,
                    user_type: user_data[0].user_type,
                    name: user_data[0].name,
                    email: user_data[0].email,
                    profile_pic: profile,
                    gender: user_data[0].gender,
                    salon_name: user_data[0].salon_name,
                    service_gender: user_data[0].service_gender,
                    address: user_data[0].address,
                    country_code: user_data[0].country_code,
                    mobile: user_data[0].mobile,
                    is_active: user_data[0].is_active,
                    is_verified: user_data[0].is_verified,
                    createdAt: user_data[0].createdAt,
                    updatedAt: user_data[0].updatedAt,
                  },
                });
              } else {
                let error = Error("Invalid password");
                error.code = 400;
                return reject(error);
              }
            })
            .catch((err) => console.log(err));
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_barber(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;

        // let id = userValidation.decode(token);
        // console.log(id);
        let { name, user_id } = body;
        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        const result = await addstaffschema.validateAsync(body);
        await SalonStaff.create({
          name: `${name}`,
          salon_id: `${user_id}`,
          createdAt: `${date}`,
          updatedAt: `${date}`,
        });

        // let select="name,salon_id";
        // let services= await SalonStaff.findAll('salon_staff',select, `salon_id=${id}`)
        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  list_barber(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, size } = body;
        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        const result = await addstaffschema.validateAsync(body);

        let getPagination = (page, size) => {
          let limit = size ? +size : 3;
          let offset = page ? (page - 1) * limit : 0;

          return { limit, offset };
        };

        let getPagingData = (data, page, limit) => {
          let { count: totalrecords, rows: SalonStaff } = data;

          return { totalrecords, SalonStaff };
        };
        let salon_staff = db.SalonStaff;
        let { limit, offset } = getPagination(page, size);
        await salon_staff
          .findAndCountAll({
            attributes: ["id", "name", "available", "createdAt", "updatedAt"],
            where: { [Op.and]: [{ salon_id: user_id }, { is_deleted: 0 }] },
            limit,
            offset,
          })
          .then((data) => {
            const response = getPagingData(data, page, limit);
            return resolve(response);
          })
          .catch((err) => console.log(err));
        console.log(limit);

        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  hasone(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let staff = await SalonUser.findAll({
          include: [SalonStaff],
        });
        return resolve(staff);
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_category(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = headers;

        let id = userValidation.decode(token);

        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        let { name } = body;
        console.log(date);
        const result = await addcategoryschema.validateAsync(body);

        let cat = await SalonCategory.create({
          name: `${name}`,
          salon_id: `${id}`,
        });

        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  get_category(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let services = await SalonCategory.findAll();

        return resolve(services);
      } catch (error) {
        return reject(error);
      }
    });
  }

  add_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;

        // let verifyToken = await userValidation.verifyToken(token);
        // let id = verifyToken.userId;

        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        let { user_id, name, price, time, description, category_id } = body;
        console.log(user_id);
        const result = await salonservicesschema.validateAsync(body);

        let services = await Service.create({
          name: `${name}`,
          price: `${price}`,
          time: `${time}`,
          description: `${description}`,
          category_id: `${category_id}`,
          salon_id: `${user_id}`,
          is_deleted: 0,
        });

        return resolve(services);
      } catch (error) {
        return reject(error);
      }
    });
  }

  get_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, size } = body;
        const result = await getserviceschema.validateAsync(body);

        let getPagination = (page, size) => {
          let limit = size ? +size : 3;
          let offset = page ? (page - 1) * limit : 0;

          return { limit, offset };
        };

        let getPagingData = (data, page, limit) => {
          let { count: totalrecords, rows: Service } = data;

          return { totalrecords, Service };
        };
        const services = db.Service;
        let { limit, offset } = getPagination(page, size);
        // let services = await Service.findAll({
        await services
          .findAndCountAll({
            where: {
              [Op.and]: [{ salon_id: `${user_id}` }, { is_deleted: 0 }],
            },
            limit,
            offset,
          })
          .then((data) => {
            const response = getPagingData(data, page, limit);
            return resolve(response);
          })
          .catch((err) => console.log(err));
        console.log(limit);
        // return resolve(services);
      } catch (error) {
        return reject(error);
      }
    });
  }

  edit_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;
        // let id = userValidation.decode(token);
        let { user_id, name, price, time, description, category_id, Id } = body;
        const result = await editcategoryschema.validateAsync(body);

        await Service.update(
          {
            name: `${name}`,
            price: `${price}`,
            time: `${time}`,
            description: `${description}`,
            category_id: `${category_id}`,
          },
          {
            where: { salon_id: `${user_id}`, id: `${Id}` },
          }
        );

        let services = await Service.findAll({
          where: {
            id: `${Id}`,
          },
        });
        console.log(services);
        return resolve(services);
      } catch (error) {
        return reject(error);
      }
    });
  }

  delete_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;

        // let verifyToken = await userValidation.verifyToken(token);
        // let id = verifyToken.userId;
        let { user_id, Id } = body;

        const result = await deleteservicesschema.validateAsync(body);

        await Service.destroy({
          where: { salon_id: `${user_id}`, id: `${Id}` },
        });

        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  list_service(req) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = req.headers;
        let id = userValidation.decode(token);

        let services = await Service.findAll({
          where: { salon_id: `${id}` },
        });
        return resolve(services);
      } catch (error) {
        return reject(error);
      }
    });
  }

  user_register(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        console.log(body);
        let {
          user_type,
          name,
          email,
          profile_pic,
          address,
          gender,
          country_code,
          mobile,
          password,
        } = body;

        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        const result = await userauthschema.validateAsync(body);
        // const result = await authschema.validateAsync(body);
        const user = await User.findAll({
          where: {
            email: `${email}`,
          },
        });
        if (user.length > 0) {
          console.log("email alredy exist");
          let error = Error("EMAIL_ALREADY_EXISTS");
          error.code = 400;
          return reject(error);
        } else if (user.length == 0) {
          bcrypt
            .hash(`${password}`, 10)
            .then(async (hash) => {
              if (hash) {
                let string = `${email}`;
                let refercode = string.substring(0, 4);
                let referalcode = referCode.custom(
                  "uppercase",
                  3,
                  5,
                  `${refercode}`
                );
                const use = await User.create({
                  user_type: `${user_type}`,
                  name: `${name}`,
                  email: `${email}`,
                  profile_pic: `${profile_pic}`,
                  gender: `${gender}`,
                  address: `${address}`,
                  country_code: `${country_code}`,
                  mobile: `${mobile}`,
                  password: `${hash}`,
                  referral_code: `${referalcode}`,
                  createdAt: `${date}`,
                  updatedAt: `${date}`,
                });

                const user_data = await User.findAll({
                  attributes: [
                    "id",
                    "user_type",
                    "name",
                    "email",
                    "profile_pic",
                    "gender",
                    "address",
                    "country_code",
                    "mobile",
                    "is_active",
                    "is_verified",
                    "referral_code",
                    "createdAt",
                    "updatedAt",
                  ],
                  where: {
                    email: `${email}`,
                  },
                });
                console.log("userdata", user_data[0]);
                const token = generateAccessToken({ user: `${email}` });
                console.log("token:", token);
                function generateAccessToken(user) {
                  return jwt.sign(
                    user,
                    (process.env.jwtSecretKey = "secretCode"),
                    { expiresIn: "15m" }
                  );
                }

                return resolve({
                  "token:": token,
                  userdata: {
                    _id: user_data[0].id,
                    user_type: user_data[0].user_type,
                    name: user_data[0].name,
                    email: user_data[0].email,
                    gender: user_data[0].gender,
                    country_code: user_data[0].country_code,
                    mobile: user_data[0].mobile,
                    is_active: user_data[0].is_active,
                    is_verified: user_data[0].is_verified,
                    referral_code: user_data[0].referral_code,
                    created_date: user_data[0].createdAt,
                    updated_date: user_data[0].updatedAt,
                  },
                });
              }
            })
            .catch((err) => console.log(err));
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  user_login(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { email, password } = body;
        const result = await userloginschema.validateAsync(body);
        const user = await User.findAll({
          where: {
            email: `${email}`,
          },
        });

        if (user.length == 0) {
          throw { message: "email is not valid", code: 403 };
        } else {
          let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");

          //query
          let user_data = await User.findAll({
            attributes: [
              "id",
              "user_type",
              "name",
              "email",
              "profile_pic",
              "password",
              "gender",
              "service_gender",
              "address",
              "country_code",
              "mobile",
              "is_active",
              "is_verified",
              "createdAt",
              "updatedAt",
            ],

            where: {
              email: `${email}`,
            },
          })
            .then(async (user_data) => {
              //compare password
              if (
                user_data &&
                (await bcrypt.compare(`${password}`, user_data[0].password))
              ) {
                const token = generateAccessToken(`${user_data[0].id}`);

                console.log("userdata", user_data);

                function generateAccessToken(userId) {
                  return jwt.sign(
                    { userId },
                    (process.env.jwtSecretKey = "secretCode"),
                    { expiresIn: "3h" }
                  );
                }

                return resolve({
                  token: token,
                  userData: {
                    _id: user_data[0].id,
                    user_type: user_data[0].user_type,
                    name: user_data[0].name,
                    email: user_data[0].email,
                    country_code: user_data[0].country_code,
                    mobile: user_data[0].mobile,
                    is_active: user_data[0].is_active,
                    is_verified: user_data[0].is_verified,
                    createdAt: user_data[0].createdAt,
                    updatedAt: user_data[0].updatedAt,
                  },
                });
              } else {
                let error = Error("Invalid password");
                error.code = 400;
                return reject(error);
              }
            })
            .catch((err) => console.log(err));
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  forgot_password(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { email } = body;
        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");

        const result = await sendotpschema.validateAsync(body);
        const user = await User.findAll({
          attributes: ["email"],
          where: {
            email: `${email}`,
          },
        }).then(async (user) => {
          if (user.length == 0) {
            console.log("email are not register");
            let error = new Error("INVALID_EMAIL");
            error.code = 400;
            return reject(error);
          } else if (user.length > 0) {
            const otp = generateOTP(6);
            console.log(otp);
            user.otp = otp;

            let transporter = nodemailer.createTransport({
              service: "gmail",
              auth: {
                user: "tristate.mteam@gmail.com",
                pass: "xcjrurbzdaxbljsa",
              },
            });

            let mailoptions = {
              from: "tristate.mteam@gmail.com",
              to: `${email}`,
              subject: "testing",
              text: `Your otp is ${otp} `,
            };

            transporter.sendMail(mailoptions);

            let sendOtp = await User.update(
              {
                otp: otp,
                is_verified: 0,
                modified_date: date,
              },
              {
                where: {
                  email: `${email}`,
                },
              }
            );
          }
        });
        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  sendotp(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { email } = body;

        const user = await User.findAll({
          attributes: ["email", "otp"],
          where: { email: email },
        });
        console.log("user", user[0].otp);
        const otp = generateOTP(6);

        user[0].otp = otp;

        let transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "tristate.mteam@gmail.com",
            pass: "xcjrurbzdaxbljsa",
          },
        });

        let mailoptions = {
          from: "tristate.mteam@gmail.com",
          to: `${email}`,
          subject: "testing",
          text: `Your otp is ${otp} `,
        };

        transporter.sendMail(mailoptions);

        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  verifyotp(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { otp, email } = body;
        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");

        const user = await User.findAll({
          attributes: ["otp"],
          where: {
            email: `${email}`,
            otp: `${otp}`,
          },
        })

          .then(async (user) => {
            if (user.length == 0) {
              let error = new Error("OTP_INVALID");
              error.code = 400;
              return reject(error);
            } else if (user.length > 0) {
              User.update(
                {
                  is_verified: 1,
                },
                {
                  where: {
                    email: `${email}`,
                  },
                }
              );

              return { is_verified: 1 };
            }
          })
          .catch((err) => console.log(err));
        return resolve(user);
      } catch (error) {
        return reject(error);
      }
    });
  }

  new_password(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { email, newpassword } = body;

        const result = await newpasswordschema.validateAsync(body);
        let user = await User.findAll({
          attributes: ["email"],
          where: {
            email: `${email}`,
          },
        });

        if (user.length == 0) {
          let error = new Error("INVALID_EMAIL");
          error.code = 400;
          return reject(error);
        } else if (user.length > 0) {
          const hash = await bcrypt.hash(`${newpassword}`, 10);
          console.log("hash", hash);
          await User.update(
            {
              password: hash,
            },
            {
              where: {
                email: `${email}`,
              },
            }
          );
          return resolve({
            userData: {
              user_type: user[0].user_type,
              name: user[0].name,
              email: user[0].email,
              gender: user[0].gender,
              country_code: user[0].country_code,
              mobile: user[0].mobile,
              is_active: user[0].is_active,
              is_verified: user[0].is_verified,
              createdAt: user[0].createdAt,
              updatedAt: user[0].updatedAt,
            },
          });
        }
      } catch (error) {
        return reject(error);
      }
    });
  }

  reset_password(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { old_password, new_password } = body;

        // let { token } = headers;
        const result = await resetpasswordschema.validateAsync(body);
        // let id = userValidation.decode(token);
        let { user_id } = body;

        //query
        let user = await User.findAll({
          attributes: ["id", "password"],
          where: {
            id: `${user_id}`,
          },
        });
        console.log(user[0].password);
        if (
          (await bcrypt.compare(`${old_password}`, user[0].password)) == false
        ) {
          let error = new Error("invalid old password");
          error.code = 400;
          return reject(error);
        }
        const hash = await bcrypt.hash(`${new_password}`, 10);
        //query

        await User.update(
          {
            password: hash,
          },
          {
            where: {
              id: `${user_id}`,
            },
          }
        );

        return resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  belongsto(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let staff = await SalonStaff.findAll({
          include: [SalonUser],
        });
        return resolve(staff);
      } catch (err) {
        return reject(err);
      }
    });
  }

  hasmany(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let staff = await SalonUser.findAll({
          include: [SalonStaff],
        });
        return resolve(staff);
      } catch (err) {
        return reject(err);
      }
    });
  }

  belongstomany(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let staff = await SalonCategory.findAll({
          include: [
            {
              model: SalonUser,
              as: "user",
              through: {
                attributes: ["id", "salon_id", "category_id"],
              },
              attributes: ["id", "name"],
            },
          ],
        });
        return resolve(staff);
      } catch (err) {
        return reject(err);
      }
    });
  }

  refreshtoken(headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let refreshtoken=config.defaultAuthToken
        // console.log(config.defaultAuthToken)
        let { token, refershtoken } = headers;
        console.log("headers", headers);

        const result = await refreshtokenschema.validateAsync(headers);
        if (headers.refreshtoken == config.defaultRefreshToken) {
          let id = userValidation.decode(token);
          const newToken = generateAccessToken(`${id}`);
          console.log("token", newToken);

          function generateAccessToken(userId) {
            return jwt.sign(
              { userId },
              (process.env.jwtSecretKey = "secretCode"),
              { expiresIn: "3h" }
            );
          }
          return resolve(newToken);
        } else {
          let err = new Error("refreshtoken is not valid");
          return reject(err);
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_businesshours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = headers;

        // let id = userValidation.decode(token);

        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        let {
          user_id,
          day,
          working_start_time,
          working_end_time,
          break_start_time,
          break_end_time,
        } = body;

        const result = await addbusshourschema.validateAsync(body);

        let businesshour = await SalonBusinessHours.findAll({
          where: {
            day: `${day}`,
            salon_id: `${user_id}`,
          },
        });
        if (businesshour.length > 0) {
          let data = {
            day: day,
            working_start_time: working_start_time,
            working_end_time: working_end_time,
            break_start_time: break_start_time,
            break_end_time: break_end_time,
          };
          await SalonBusinessHours.update(data, {
            where: { salon_id: `${user_id}`, day: `${day}` },
          });
        } else if (businesshour.length == 0) {
          let salonbusshour = await SalonBusinessHours.create({
            day: `${day}`,
            working_start_time: `${working_start_time}`,
            working_end_time: `${working_end_time}`,
            break_start_time: `${break_start_time}`,
            break_end_time: `${break_end_time}`,
            salon_id: `${user_id}`,
          });
          return resolve({ salonbusshour: salonbusshour });
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  edit_businesshours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = headers;
        let id = userValidation.decode(token);
        let {
          day,
          working_start_time,
          working_end_time,
          break_start_time,
          break_end_time,
        } = body;
        const result = await editbusinesshourschema.validateAsync(body);
        await SalonBusinessHours.update(
          {
            day: `${day}`,
            working_start_time: `${working_start_time}`,
            working_end_time: `${working_end_time}`,
            break_start_time: `${break_start_time}`,
            break_end_time: `${break_end_time}`,
            salon_id: `${id}`,
          },
          {
            where: { salon_id: `${id}`, day: `${day}` },
          }
        );
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_weeklybusinesshour(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = headers;

        let { user_id } = body;
        const result = await listbusinesshourschema.validateAsync(body);

        // let DAY=[{'day':1},{'day':2},{'day':3},{'day':4},{'day':5},{'day':6},{'day':7}]
        let i;
        let DAY = [];
        let hour = [];
        for (let i = 1; i <= 7; i++) {
          DAY.push({ day: i });

          console.log("i", i);
          const busshour = await SalonBusinessHours.findOne({
            where: { salon_id: `${user_id}`, day: i },
          });

          if (busshour) {
            hour.push({
              day: busshour.day,
              working_start_time: busshour.working_start_time,
              working_end_time: busshour.working_end_time,
              break_start_time: busshour.break_start_time,
              break_end_time: busshour.break_end_time,
            });
          } else {
            hour.push({
              day: i,
              working_start_time: " ",
              working_end_time: " ",
              break_start_time: " ",
              break_end_time: " ",
            });
          }
        }
        return resolve({ busshour: hour });
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_dailybusinesshour(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = headers;

        let { user_id, date, schedule_type, day } = body;
        const result = await listdailybusinesshour.validateAsync(body);

        //query
        const busshour = await SalonBusinessHours.findOne({
          attributes: [
            "_id",
            "working_start_time",
            "working_end_time",
            "break_start_time",
            "break_end_time",
            "available",
          ],
          where: {
            salon_id: `${user_id}`,
            schedule_type: `${schedule_type}`,
            date: `${date}`,
          },
        });

        if (busshour == null) {
          //query
          const scheduleData = await SalonBusinessHours.findOne({
            attributes: [
              "_id",
              "working_start_time",
              "working_end_time",
              "break_start_time",
              "break_end_time",
              "available",
            ],
            where: { salon_id: `${user_id}`, schedule_type: 1, day: `${day}` },
          });

          return resolve(scheduleData);
        }

        return resolve(busshour);
      } catch (err) {
        return reject(err);
      }
    });
  }

  weeklydailyschedule(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { token } = headers;

        // let id = userValidation.decode(token);

        let {
          user_id,
          schedule_type,
          day,
          working_start_time,
          working_end_time,
          break_start_time,
          break_end_time,
          date,
        } = body;

        // let busHour = await SalonBusinessHours.findAll({
        //   where: {
        //     day: `${day}`,
        //     salon_id: `${user_id}`,
        //   },
        // });

        // console.log('data',busHour)

        if (schedule_type == 1) {
          await weeklyscheduleschema.validateAsync(body);
          let weekly = await SalonBusinessHours.findAll({
            where: {
              day: `${day}`,
              salon_id: `${user_id}`,
            },
          });
          if (weekly.length > 0) {
            let data = {
              schedule_type: schedule_type,
              day: day,
              working_start_time: working_start_time,
              working_end_time: working_end_time,
              break_start_time: break_start_time,
              break_end_time: break_end_time,
            };
            await SalonBusinessHours.update(data, {
              where: {
                salon_id: `${user_id}`,

                day: `${day}`,
              },
            });
          } else if (weekly.length == 0) {
            let salonbusshour = await SalonBusinessHours.create({
              schedule_type: `${schedule_type}`,
              day: `${day}`,
              working_start_time: `${working_start_time}`,
              working_end_time: `${working_end_time}`,
              break_start_time: `${break_start_time}`,
              break_end_time: `${break_end_time}`,
              salon_id: `${user_id}`,
            });
            return resolve({ salonbusshour: salonbusshour });
          }
        } else {
          await dailyscheduleschema.validateAsync(body);
          let dailyHour = await SalonBusinessHours.findAll({
            where: {
              date: `${date}`,
              salon_id: `${user_id}`,
            },
          });
          console.log("a", dailyHour);

          if (dailyHour.length == 0) {
            let dailyhourdata = await SalonBusinessHours.create({
              schedule_type: `${schedule_type}`,
              working_start_time: `${working_start_time}`,
              working_end_time: `${working_end_time}`,
              break_start_time: `${break_start_time}`,
              break_end_time: `${break_end_time}`,
              salon_id: `${user_id}`,
              date: `${date}`,
            });
            return resolve({ dailyhourdata: dailyhourdata });
          } else {
            let Data = {
              schedule_type: schedule_type,
              working_start_time: working_start_time,
              working_end_time: working_end_time,
              break_start_time: break_start_time,
              break_end_time: break_end_time,
              date: date,
            };
            await SalonBusinessHours.update(Data, {
              where: {
                salon_id: `${user_id}`,
                schedule_type: `${schedule_type}`,
              },
            });
          }
        }

        // return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_barberbusinesshours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;

        // let id = userValidation.decode(token);

        let {
          user_id,
          barber_id,
          schedule_type,
          date,
          day,
          working_start_time,
          working_end_time,
          break_start_time,
          break_end_time,
          salonworking_start_time,
          salonworking_end_time,
        } = body;

        let workingstarttime = convertTimeFormat(`${working_start_time}`);
        let workingendtime = convertTimeFormat(`${working_end_time}`);
        let breakendtime = convertTimeFormat(`${break_end_time}`);
        let breakstarttime = convertTimeFormat(`${break_start_time}`);

        console.log("schedule_type", `${schedule_type}`);

        if (schedule_type == 1) {
          let businesshour = await SalonBusinessHours.findAll({
            attributes: ["working_start_time", "working_end_time"],
            where: {
              day: `${day}`,
              salon_id: `${user_id}`,
              // barber_id: `${barber_id}`
            },
          });
          console.log("businesshour", businesshour);
          let bussworkstarttime = convertTimeFormat(
            `${businesshour[0].working_start_time}`
          );
          let bussworkendtime = convertTimeFormat(
            `${businesshour[0].working_end_time}`
          );

          await addstaffweekbusshour.validateAsync(body);
          let barberweekbusshour = await StaffBusinessHours.findAll({
            where: {
              day: `${day}`,
              barber_id: `${barber_id}`,
              salon_id: `${user_id}`,
            },
          });

          if (barberweekbusshour.length > 0) {
            if (
              bussworkstarttime < workingstarttime &&
              bussworkendtime > workingstarttime &&
              bussworkendtime > workingendtime &&
              bussworkstarttime < workingendtime &&
              breakstarttime > workingstarttime &&
              breakstarttime < workingendtime &&
              breakendtime < workingendtime &&
              breakendtime > workingstarttime &&
              workingstarttime < workingendtime &&
              breakstarttime < breakendtime
            ) {
              let data = {
                barber_id: barber_id,
                day: day,
                working_start_time: working_start_time,
                working_end_time: working_end_time,
                break_start_time: break_start_time,
                break_end_time: break_end_time,
              };
              await StaffBusinessHours.update(data, {
                where: { salon_id: `${user_id}`, day: `${day}` },
              });
            } else {
              let error = new Error("this time is not allow");
              return reject(error);
            }
          } else {
            if (
              bussworkstarttime < workingstarttime &&
              bussworkendtime > workingstarttime &&
              bussworkendtime > workingendtime &&
              bussworkstarttime < workingendtime &&
              breakstarttime > workingstarttime &&
              breakstarttime < workingendtime &&
              breakendtime < workingendtime &&
              breakendtime > workingstarttime &&
              workingstarttime < workingendtime &&
              breakstarttime < breakendtime
            ) {
              let staffbusshour = await StaffBusinessHours.create({
                day: `${day}`,
                barber_id: `${barber_id}`,
                working_start_time: `${working_start_time}`,
                working_end_time: `${working_end_time}`,
                break_start_time: `${break_start_time}`,
                break_end_time: `${break_end_time}`,
                salon_id: `${user_id}`,
              });
              return resolve({ staffbusshour: staffbusshour });
            } else {
              let error = new Error("this time is not allow");
              return reject(error);
            }
          }
        } else {
          let businessdailyhour = await SalonBusinessHours.findAll({
            attributes: ["working_start_time", "working_end_time"],
            where: {
              date: `${date}`,
              salon_id: `${user_id}`,
              // barber_id: `${barber_id}`
            },
          });

          let bussdailystarttime = convertTimeFormat(
            `${businessdailyhour[0].working_start_time}`
          );
          let bussdailyendtime = convertTimeFormat(
            `${businessdailyhour[0].working_end_time}`
          );

          await addstaffdailybusshour.validateAsync(body);
          let barberdailybusshour = await StaffBusinessHours.findAll({
            where: {
              date: `${date}`,
              barber_id: `${barber_id}`,
              salon_id: `${user_id}`,
            },
          });

          if (barberdailybusshour.length > 0) {
            if (
              bussdailystarttime < workingstarttime &&
              bussdailyendtime > workingstarttime &&
              bussdailyendtime > workingendtime &&
              bussdailystarttime < workingendtime &&
              breakstarttime > workingstarttime &&
              breakstarttime < workingendtime &&
              breakendtime < workingendtime &&
              breakendtime > workingstarttime &&
              workingstarttime < workingendtime &&
              breakstarttime < breakendtime
            ) {
              let data = {
                schedule_type: schedule_type,
                barber_id: barber_id,
                date: date,
                working_start_time: working_start_time,
                working_end_time: working_end_time,
                break_start_time: break_start_time,
                break_end_time: break_end_time,
              };
              await StaffBusinessHours.update(data, {
                where: {
                  salon_id: `${user_id}`,
                  schedule_type: `${schedule_type}`,
                  date: `${date}`,
                },
              });
            } else {
              let error = new Error("this time is not allow");
              return reject(error);
            }
          } else {
            if (
              bussdailystarttime < workingstarttime &&
              bussdailyendtime > workingstarttime &&
              bussdailyendtime > workingendtime &&
              bussdailystarttime < workingendtime &&
              breakstarttime > workingstarttime &&
              breakstarttime < workingendtime &&
              breakendtime < workingendtime &&
              breakendtime > workingstarttime &&
              workingstarttime < workingendtime &&
              breakstarttime < breakendtime
            ) {
              let staffdailybusshour = await StaffBusinessHours.create({
                date: `${date}`,
                barber_id: `${barber_id}`,
                working_start_time: `${working_start_time}`,
                working_end_time: `${working_end_time}`,
                break_start_time: `${break_start_time}`,
                break_end_time: `${break_end_time}`,
                salon_id: `${user_id}`,
                schedule_type: `${schedule_type}`,
              });
              return resolve({ staffdailybusshour: staffdailybusshour });
            } else {
              let error = new Error("this time is not allow");
              return reject(error);
            }
          }
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_barberdailyhours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;

        let { user_id, date, schedule_type, day } = body;
        const result = await listdailybusinesshour.validateAsync(body);

        //query
        const busshour = await StaffBusinessHours.findOne({
          attributes: [
            "_id",
            "working_start_time",
            "working_end_time",
            "break_start_time",
            "break_end_time",
            "available",
            "barber_id",
          ],
          where: {
            salon_id: `${user_id}`,
            schedule_type: `${schedule_type}`,
            date: `${date}`,
          },
        });

        if (busshour == null) {
          //query
          const scheduleData = await StaffBusinessHours.findOne({
            attributes: [
              "_id",
              "working_start_time",
              "working_end_time",
              "break_start_time",
              "break_end_time",
              "available",
              "barber_id",
            ],
            where: { salon_id: `${user_id}`, schedule_type: 1, day: `${day}` },
          });

          return resolve(scheduleData);
        }

        return resolve(busshour);
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_barberweeklyhours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { token } = headers;

        let { user_id } = body;
        const result = await listbusinesshourschema.validateAsync(body);

        // let DAY=[{'day':1},{'day':2},{'day':3},{'day':4},{'day':5},{'day':6},{'day':7}]
        let i;
        let DAY = [];
        let hour = [];
        for (let i = 1; i <= 7; i++) {
          DAY.push({ day: i });

          console.log("i", i);
          const busshour = await StaffBusinessHours.findOne({
            where: { salon_id: `${user_id}`, day: i },
          });

          if (busshour) {
            hour.push({
              day: busshour.day,
              working_start_time: busshour.working_start_time,
              working_end_time: busshour.working_end_time,
              break_start_time: busshour.break_start_time,
              break_end_time: busshour.break_end_time,
            });
          } else {
            hour.push({
              day: i,
              working_start_time: " ",
              working_end_time: " ",
              break_start_time: " ",
              break_end_time: " ",
            });
          }
        }
        return resolve({ busshour: hour });
      } catch (err) {
        return reject(err);
      }
    });
  }

  edit_userprofile(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, name, email, country_code, mobile, gender } = body;
        const result = await edituserprofile.validateAsync(body);
        await User.update(
          {
            name: `${name}`,
            email: `${email}`,
            country_code: `${country_code}`,
            mobile: `${mobile}`,
            gender: `${gender}`,
            // salon_id: `${user_id}`,
          },
          {
            where: { id: `${user_id}`, user_type: "1" },
          }
        );

        let user = await User.findOne({
          attributes: ["name", "email", "country_code", "mobile", "gender"],

          where: { id: `${user_id}`, user_type: 1 },
        });
        return resolve(user);
      } catch (err) {
        return reject(err);
      }
    });
  }

  contactus(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, subject, message } = body;
        const result = await contactusschema.validateAsync(body);
        await ContactUs.create({
          users_id: `${user_id}`,
          subject: `${subject}`,
          message: `${message}`,
        });

        // let user = await User.findOne(
        //   {
        //     attributes: []

        //   ,
        //     where: { id: `${user_id}`, user_type: 1 },
        //   }
        // );
        // return resolve(user);
      } catch (err) {
        return reject(err);
      }
    });
  }

  search(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id,
          service_gender,
          salon_name,
          salon_type,
          priceFrom,
          priceTo,
          latitude,
          longitude,
          distanceFrom,
          distanceTo,
          page,
          size,
        } = body;
        const result = await searchschema.validateAsync(body);

       
        let distance = 5
        

        if (latitude !== null && longitude !== null && distance !== null) {
          //query_near_salon_distance_find
          const salon_by_location = await User.scope({
            method: ["distance", latitude, longitude, distance],
          }).findAll({
            attributes: ["id"],
            where: {
              user_type: 2,
            },
            order: Sequelize.col("distance"),
            limit: 5,
          });

          let array_dist = [];
          console.log("distance", salon_by_location[0].dataValues.distance);
          for (let i in salon_by_location) {
            if (!array_dist.includes(salon_by_location[i].dataValues.id)) {
              array_dist.push({
                distance: salon_by_location[i].dataValues.distance,
                id: salon_by_location[i].dataValues.id,
              });
            }
          }

          //filter_radius_miles
          let array_distance = [];
          var salon_dist = array_dist.filter((a) => {
            let dist = a.distance;
            let id = a.id;
            console.log(dist >= distanceFrom && dist <= distanceTo);
            if (dist >= distanceFrom && dist <= distanceTo == true) {
              return array_distance.push({id:id});
            }
          });
          console.log("r", array_distance);

          let array_locate = [];
          let array1 = [];

          for (let i in array_distance) {
            if (!array_locate.includes(array_distance[i].id)) {
              array_locate.push(array_distance[i].id);
            }
          }
          console.log("r", array_locate);

          // ------------is_favourite_salon
          //query
          let saloninfo = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            include: [
              {
                model: UserFavouriteSalons,
                as: "favsalon",
                attributes: ["id", "is_favourite"],
                where: {
                  salon_id: array_locate,
                  users_id: `${user_id}`,
                },
              },
            ],

            where: {
              [Op.and]: [
                { id: array_locate },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          //query_salon_detail_list
          let salon_user = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],

            where: {
              [Op.and]: [
                { id: array_locate },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          for (let k in saloninfo) {
            let profile = profile_pic(saloninfo[k].profile_pic);
            if (!array1.includes(saloninfo[k].id)) {
              // if(saloninfo[k].favsalon[0].is_favourite == 1){
              array1.push({
                id: saloninfo[k].id,
                address: saloninfo[k].address,
                salon_name: saloninfo[k].salon_name,
                salon_type: saloninfo[k].salon_type,
                profile_pic: profile,
                counts: saloninfo[k].counts,
                ratings: saloninfo[k].ratings,
                is_favourite: saloninfo[k].favsalon[0].is_favourite,
              });
            }
          }

          let array_isfav_id = [];

          for (let g in array1) {
            if (!array_isfav_id.includes(array1[g].id)) {
              array_isfav_id.push(array1[g].id);
            }
          }
          //for_salon_is_fav_null
          for (let d in salon_user) {
            if (!array_isfav_id.includes(salon_user[d].id)) {
              let profile = profile_pic(salon_user[d].profile_pic);

              {
                array1.push({
                  id: salon_user[d].id,
                  address: salon_user[d].address,
                  salon_name: salon_user[d].salon_name,
                  salon_type: salon_user[d].salon_type,
                  profile_pic: profile,
                  counts: salon_user[d].counts,
                  ratings: salon_user[d].ratings,

                  is_favourite: null,
                });
              }
            }
          }

          //query_service_price_filter
          let salonservice = await Service.findAll({
            include: [
              {
                as: "salon",
                model: SalonUser,
                attributes: [
                  "id",
                  "salon_type",
                  "profile_pic",
                  "salon_name",
                  "address",
                  "counts",
                  "ratings",
                ],
              },
            ],
            attributes: [],
            where: {
              salon_id: array_locate,
              price: {
                [Op.between]: [priceFrom, priceTo],
              },
            },
          });

          let array_id = [];
          let array_data = [];

          for (let m in salonservice) {
            let profile = profile_pic(salonservice[m].salon.profile_pic);
            if (!array_id.includes(salonservice[m].salon.id)) {
              array_id.push(salonservice[m].salon.id);
            }
          }

          //query
          let salon_fav = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            include: [
              {
                model: UserFavouriteSalons,
                as: "favsalon",
                attributes: ["id", "is_favourite"],
                where: {
                  salon_id: array_id,
                  users_id: `${user_id}`,
                },
              },
            ],

            where: {
              [Op.and]: [
                { id: array_locate },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          //  -----------------filter_distance,dist_data
          let dist_data = [];
          //query
          let salon_loc_fav = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            include: [
              {
                model: UserFavouriteSalons,
                as: "favsalon",
                attributes: ["id", "is_favourite"],
                where: {
                  salon_id: array_locate,
                  users_id: `${user_id}`,
                },
              },
            ],

            where: {
              [Op.and]: [
                { id: array_locate },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });
          console.log("salon_loc_fav", salon_loc_fav);

          for (let h in salon_loc_fav) {
            console.log("salon_loc_fav", salon_loc_fav[h].id);
            let profile = profile_pic(salon_loc_fav[h].profile_pic);
            dist_data.push({
              id: salon_loc_fav[h].id,
              address: salon_loc_fav[h].address,
              salon_name: salon_loc_fav[h].salon_name,
              salon_type: salon_loc_fav[h].salon_type,
              profile_pic: profile,
              counts: salon_loc_fav[h].counts,
              ratings: salon_loc_fav[h].ratings,
              is_favourite: salon_loc_fav[h].favsalon[0].is_favourite,
            });
          }

          let loc_null = [];
          for (let h in salon_loc_fav) {
            if (!loc_null.includes(salon_loc_fav[h].id))
              loc_null.push(salon_loc_fav[h].id);
          }

          let salon_loc_is_fev = await SalonUser.findAll({
            where: {
              [Op.and]: [
                { id: array_locate },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          for (let h in salon_loc_is_fev) {
            console.log("salon_loc_fav", salon_loc_is_fev[h].id);

            let profile = profile_pic(salon_loc_is_fev[h].profile_pic);
            if (!loc_null.includes(salon_loc_is_fev[h].id)) {
              dist_data.push({
                id: salon_loc_is_fev[h].id,
                address: salon_loc_is_fev[h].address,
                salon_name: salon_loc_is_fev[h].salon_name,
                salon_type: salon_loc_is_fev[h].salon_type,
                profile_pic: profile,
                counts: salon_loc_is_fev[h].counts,
                ratings: salon_loc_is_fev[h].ratings,
                is_favourite: null,
              });
            }
          }

          let array_filter_dist_id = [];

          for (let g in dist_data) {
            if (!array_filter_dist_id.includes(dist_data[g].id)) {
              array_filter_dist_id.push(dist_data[g].id);
            }
          }
          console.log("array_filter_dist_id", array_filter_dist_id);
          //query
          let salon_data = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            where: {
              [Op.and]: [
                { id: array_id },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          for (let d in salon_data) {
            if (!array_filter_dist_id.includes(salon_data[d].id)) {
              let profile = profile_pic(salon_data[d].profile_pic);
              {
                array_data.push({
                  id: salon_data[d].id,
                  address: salon_data[d].address,
                  salon_name: salon_data[d].salon_name,
                  salon_type: salon_data[d].salon_type,
                  profile_pic: profile,
                  counts: salon_data[d].counts,
                  ratings: salon_data[d].ratings,

                  is_favourite: null,
                });
              }
            }
          }
          console.log("dist_data", dist_data);

          //  -----------------filter_price_by_range,array_data
          for (let h in salon_fav) {
            let profile = profile_pic(salon_fav[h].profile_pic);

            // if (!array_id.includes(salon_fav[h].id)) {
            array_data.push({
              id: salon_fav[h].id,
              address: salon_fav[h].address,
              salon_name: salon_fav[h].salon_name,
              salon_type: salon_fav[h].salon_type,
              profile_pic: profile,
              counts: salon_fav[h].counts,
              ratings: salon_fav[h].ratings,
              is_favourite: salon_fav[h].favsalon[0].is_favourite,
            });
            // }
          }

          let array_filter_id = [];

          for (let g in array_data) {
            if (!array_filter_id.includes(array_data[g].id)) {
              array_filter_id.push(array_data[g].id);
            }
          }

          //query
          let user = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            where: {
              [Op.and]: [
                { id: array_id },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          for (let d in user) {
            if (!array_filter_id.includes(user[d].id)) {
              let profile = profile_pic(user[d].profile_pic);
              {
                array_data.push({
                  id: user[d].id,
                  address: user[d].address,
                  salon_name: user[d].salon_name,
                  salon_type: user[d].salon_type,
                  profile_pic: profile,
                  counts: user[d].counts,
                  ratings: user[d].ratings,

                  is_favourite: null,
                });
              }
            }
          }

          let price_dist_filter = [];
          
          console.log("array_filter_id", array_filter_id);

          let array_id_merge = [];
          for (let g in array_data) {
            // if (!array_filter_id.includes(price_dist_filter[g].id)){
            array_id_merge.push(
              array_data[g].id,
            );
            // }
          }
          console.log("array_id_merge", array_id_merge);

          for (let g in array_data) {
            if (!array_filter_id.includes(array_data[g].id)){

            price_dist_filter.push({
              id: array_data[g].id,
              address: array_data[g].address,
              salon_name: array_data[g].salon_name,
              salon_type: array_data[g].salon_type,
              profile_pic: array_data[g].profile,
              counts: array_data[g].counts,
              ratings: array_data[g].ratings,

              is_favourite: array_data[g].is_favourite,
            });
            }
          }

          for (let g in dist_data) {
            if (!array_id_merge.includes(dist_data[g].id)) {
              price_dist_filter.push(dist_data);
            }
          }
          console.log("dist_data", dist_data);
          //dist_data
          if (priceFrom && priceTo && distanceFrom && distanceTo) {
            return resolve(paginatedata(price_dist_filter, size, page));
          } else if (priceFrom && priceTo) {
            return resolve(paginatedata(array_data, size, page));
          } else if (distanceFrom && distanceTo) {
            return resolve(paginatedata(dist_data, size, page));
          } else {
            return resolve(paginatedata(array1, size, page));
          }
        }

        //-----------------------------------------
        else {
          let array = [];
          let array1 = [];
          let salon = await SalonUser.findAll({
            attributes: ["id"],

            where: {
              salon_name: { [Op.iLike]: `${salon_name}%` },
              user_type: 2,
              [Op.or]: [
                { service_gender: `${service_gender}` },
                { service_gender: 3 },
              ],
              salon_type: `${salon_type}`,
            },
          });

          console.log("salon", array);

          let salonStaff = await SalonStaff.findAll({
            attributes: ["salon_id"],
            where: {
              name: { [Op.iLike]: `${salon_name}%` },
            },
          });
          console.log("salonStaff", salonStaff);

          for (let i in salonStaff) {
            if (!array.includes(salonStaff[i].salon_id)) {
              array.push(salonStaff[i].salon_id);
              console.log("salonStf", salonStaff[i].salon_id);
            }
          }
          for (let j in salon) {
            if (!array.includes(salon[j].id)) {
              array.push(salon[j].id);
              console.log("salonff", salon[j].id);
            }
          }
          console.log("arraysearch", array);

          //-------------------
          let saloninfo = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            include: [
              {
                model: UserFavouriteSalons,
                as: "favsalon",
                attributes: ["id", "is_favourite"],
                where: {
                  salon_id: array,
                  users_id: `${user_id}`,
                },
              },
            ],

            where: {
              [Op.and]: [
                { id: array },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          console.log("salonStaff", saloninfo);
          let salon_user = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],

            where: {
              [Op.and]: [
                { id: array },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });

          for (let k in saloninfo) {
            let profile = profile_pic(saloninfo[k].profile_pic);
            console.log("salonStaff", profile);
            if (!array1.includes(saloninfo[k].id)) {
              // if(saloninfo[k].favsalon[0].is_favourite == 1){
              array1.push({
                id: saloninfo[k].id,
                address: saloninfo[k].address,
                salon_name: saloninfo[k].salon_name,
                salon_type: saloninfo[k].salon_type,
                profile_pic: profile,
                counts: saloninfo[k].counts,
                ratings: saloninfo[k].ratings,
                is_favourite: saloninfo[k].favsalon[0].is_favourite,
              });
            }
          }

          let array_isfav_id = [];

          for (let g in array1) {
            if (!array_isfav_id.includes(array1[g].id)) {
              array_isfav_id.push(array1[g].id);
            }
          }
          console.log("array_isfav_id", array_isfav_id);
          for (let d in salon_user) {
            if (!array_isfav_id.includes(salon_user[d].id)) {
              let profile = profile_pic(salon_user[d].profile_pic);
              console.log("salonStaff", profile);
              {
                array1.push({
                  id: salon_user[d].id,
                  address: salon_user[d].address,
                  salon_name: salon_user[d].salon_name,
                  salon_type: salon_user[d].salon_type,
                  profile_pic: profile,
                  counts: salon_user[d].counts,
                  ratings: salon_user[d].ratings,

                  is_favourite: null,
                });
              }
            }
          }

          let salonservice = await Service.findAll({
            include: [
              {
                as: "salon",
                model: SalonUser,
                attributes: [
                  "id",
                  "salon_type",
                  "profile_pic",
                  "salon_name",
                  "address",
                  "counts",
                  "ratings",
                ],
              },
            ],
            attributes: [],
            where: {
              salon_id: array,
              price: {
                [Op.between]: [priceFrom, priceTo],
              },
            },
          });

          let array_id = [];
          let array_data = [];

          for (let m in salonservice) {
            let profile = profile_pic(salonservice[m].salon.profile_pic);
            if (!array_id.includes(salonservice[m].salon.id)) {
              array_id.push(salonservice[m].salon.id);
            }
          }
          console.log("array_id", array_id);
          let salon_fav = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            include: [
              {
                model: UserFavouriteSalons,
                as: "favsalon",
                attributes: ["id", "is_favourite"],
                where: {
                  salon_id: array_id,
                  users_id: `${user_id}`,
                },
              },
            ],

            where: {
              [Op.and]: [
                { id: array },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });
          console.log("salon_fav", salon_fav[0].favsalon[0].is_favourite);

          for (let h in salon_fav) {
            let profile = profile_pic(salon_fav[h].profile_pic);

            // if (!array_id.includes(salon_fav[h].id)) {
            array_data.push({
              id: salon_fav[h].id,
              address: salon_fav[h].address,
              salon_name: salon_fav[h].salon_name,
              salon_type: salon_fav[h].salon_type,
              profile_pic: profile,
              counts: salon_fav[h].counts,
              ratings: salon_fav[h].ratings,
              is_favourite: salon_fav[h].favsalon[0].is_favourite,
            });
            // }
          }

          let array_filter_id = [];

          for (let g in array_data) {
            if (!array_filter_id.includes(array_data[g].id)) {
              array_filter_id.push(array_data[g].id);
            }
          }
          console.log("array_filter_id", array_filter_id);

          let user = await SalonUser.findAll({
            attributes: [
              "id",
              "salon_type",
              "profile_pic",
              "salon_name",
              "address",
              "counts",
              "ratings",
            ],
            where: {
              [Op.and]: [
                { id: array_id },
                {
                  service_gender: `${service_gender}`,
                  salon_type: `${salon_type}`,
                },
              ],
            },
          });
          console.log("array_filter_id", array_filter_id);
          for (let d in user) {
            if (!array_filter_id.includes(user[d].id)) {
              let profile = profile_pic(user[d].profile_pic);
              console.log("salonStaff", profile);
              {
                array_data.push({
                  id: user[d].id,
                  address: user[d].address,
                  salon_name: user[d].salon_name,
                  salon_type: user[d].salon_type,
                  profile_pic: profile,
                  counts: user[d].counts,
                  ratings: user[d].ratings,

                  is_favourite: null,
                });
              }
            }
          }
          if (priceFrom && priceTo) {
            return resolve(paginatedata(array_data, size, page));
          } else {
            return resolve(paginatedata(array1, size, page));
          }
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_writereview(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id,
          salon_id,
          price,
          value,
          quality,
          friendliness,
          cleanliness,
          review_description,
        } = body;
        const result = await writereviewschema.validateAsync(body);
        //query
        let alredyreviewed = await WriteReview.findOne({
          where: {
            [Op.and]: [{ users_id: `${user_id}` }, { salon_id: `${salon_id}` }],
          },
        });

        if (alredyreviewed) {
          let data = {
            price: `${price}`,
            value: `${value}`,
            quality: `${quality}`,
            friendliness: `${friendliness}`,
            cleanliness: `${cleanliness}`,
            review_description: `${review_description}`,
          };
          await WriteReview.update(data, {
            where: {
              [Op.and]: [
                { users_id: `${user_id}` },
                { salon_id: `${salon_id}` },
              ],
            },
          });
          let list = await db.sequelize.query(
            `SELECT  (avg(price)+avg(value)+avg(quality)+avg(friendliness)+avg(cleanliness))/5 as avragerate from write_reviews where salon_id=${salon_id} `
          );

          let count = await db.sequelize.query(
            `SELECT count(salon_id) as totalreview from write_reviews where salon_id=${salon_id}`
          );

          await db.sequelize.query(
            `UPDATE users set ratings = round(${list[0][0].avragerate}, 2),counts=${count[0][0].totalreview}  where id=${salon_id}`
          );
          //query
          let findreview = await WriteReview.findOne({
            where: {
              [Op.and]: [
                { users_id: `${user_id}` },
                { salon_id: `${salon_id}` },
              ],
            },
          });
          return resolve(findreview);
        } else {
          let data = {
            users_id: `${user_id}`,
            salon_id: `${salon_id}`,
            price: `${price}`,
            value: `${value}`,
            quality: `${quality}`,
            friendliness: `${friendliness}`,
            cleanliness: `${cleanliness}`,
            review_description: `${review_description}`,
          };
          //query
          let writereview = await WriteReview.create(data, {
            where: {
              users_id: `${user_id}`,
            },
          });

          let list = await db.sequelize.query(
            `SELECT  (avg(price)+avg(value)+avg(quality)+avg(friendliness)+avg(cleanliness))/5 as avragerate from write_reviews where salon_id=${salon_id} `
          );

          let count = await db.sequelize.query(
            `SELECT count(salon_id) as totalreview from write_reviews where salon_id=${salon_id}`
          );

          await db.sequelize.query(
            `UPDATE users set ratings = round(${list[0][0].avragerate}, 2),counts=${count[0][0].totalreview}  where id=${salon_id}`
          );

          return resolve(writereview);
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_review(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, size } = body;
        // const result = await contactusschema.validateAsync(body);

        const userType = await User.findOne({
          attributes: ["user_type"],
          where: { id: `${user_id}` },
        });
        console.log("user_id", userType.dataValues.user_type);

        //query
        if (userType.dataValues.user_type == 2) {
          let listofreview = await SalonUser.findAndCountAll({
            attributes: [
              "id",
              "user_type",
              "salon_name",
              "profile_pic",
              "salon_type",
              "service_gender",
            ],
            include: [
              {
                model: User,
                as: "user",
                through: {
                  attributes: [
                    "id",
                    "price",
                    "value",
                    "quality",
                    "friendliness",
                    "cleanliness",
                    "review_description",
                    "createdAt",
                  ],
                },
                attributes: ["id", "name"],
              },
            ],
            where: {
              id: `${user_id}`,
            },
          });
          return resolve({ listofreviews: listofreview });
        } else {
          let user_review = await User.findAndCountAll({
            // include: [{ model: WriteReview, as: "write_reviews" }],
            // as: "salon_reviews",
            attributes: ["id", "name"],
            include: [
              {
                model: SalonUser,
                as: "user",
                through: {
                  attributes: [
                    "id",
                    "price",
                    "value",
                    "quality",
                    "friendliness",
                    "cleanliness",
                    "review_description",
                    "createdAt",
                  ],
                },
                attributes: [
                  "id",
                  "user_type",
                  "salon_name",
                  "profile_pic",
                  "salon_type",
                  "service_gender",
                ],
              },
            ],
            where: {
              id: `${user_id}`,
            },
          });
          return resolve({ user_review: user_review });
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_userfavouritesalon(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id, is_deleted } = body;
        const result = await add_userfavouritesalon.validateAsync(body);
        let date = moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        //query
        let alredyfavouritesalon = await UserFavouriteSalons.findOne({
          where: {
            [Op.and]: [{ users_id: `${user_id}` }, { salon_id: `${salon_id}` }],
          },
        });
        console.log("alredyfavouritesalon", alredyfavouritesalon);
        if (alredyfavouritesalon && is_deleted == 0) {
          // let data = {

          // };
          //query
          await UserFavouriteSalons.update(
            {
              users_id: `${user_id}`,
              salon_id: `${salon_id}`,
              is_favourite: 1,
              is_deleted: 0,
              updatedAt: `${date}`,
            },
            {
              where: {
                [Op.and]: [
                  { users_id: `${user_id}` },
                  { salon_id: `${salon_id}` },
                ],
              },
            }
          );
          //query
          let findfavouritesalon = await UserFavouriteSalons.findOne({
            where: {
              [Op.and]: [
                { users_id: `${user_id}` },
                { salon_id: `${salon_id}` },
              ],
            },
          });
          return resolve(findfavouritesalon);
        } else if (is_deleted == 1) {
          let data = {
            is_deleted: 1,
            is_favourite: 0,
          };
          //query
          await UserFavouriteSalons.update(
            data,

            {
              where: {
                [Op.and]: [
                  { users_id: `${user_id}` },
                  { salon_id: `${salon_id}` },
                ],
              },
            }
          );
        } else {
          let data = {
            users_id: `${user_id}`,
            salon_id: `${salon_id}`,
            is_favourite: 1,
            is_deleted: 0,
            createdAt: `${date}`,
            updatedAt: `${date}`,
          };
          //query
          let addfavouritesalon = await UserFavouriteSalons.create(data, {
            where: {
              users_id: `${user_id}`,
              salon_id: `${salon_id}`,
            },
          });

          return resolve(addfavouritesalon);
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_userfavouritesalon(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, size } = body;

        let getPagination = (page, size) => {
          let limit = size ? +size : 3;
          let offset = page ? (page - 1) * limit : 0;

          return { limit, offset };
        };

        let getPagingData = (data, page, limit) => {
          let { count: totalrecords, rows: UserFavouriteSalons } = data;

          return { totalrecords, UserFavouriteSalons };
        };
        //query
        const listfavouritesalon = db.UserFavouriteSalons;
        let { limit, offset } = getPagination(page, size);
        await listfavouritesalon
          .findAndCountAll({
            attributes: ["is_favourite"],
            include: [
              {
                model: SalonUser,
                attributes: [
                  "id",
                  "salon_name",
                  "profile_pic",
                  "salon_type",
                  "service_gender",
                  "address",
                  "ratings",
                ],
              },
            ],

            where: {
              users_id: `${user_id}`,
              is_favourite: 1,
            },
            limit,
            offset,
          })

          .then((data) => {
            const response = getPagingData(data, page, limit);
            return resolve(response);
          })
          .catch((err) => console.log(err));
        console.log(limit);

        // return resolve({ listfavouritesalon: listfavouritesalon });
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_salondetail(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, details } = body;
        console.log(user_id);
        const result = await add_salondetail.validateAsync(body);
        //query
        let alredysalondetail = await SalonUser.findOne({
          where: {
            id: `${user_id}`,
          },
        });
        console.log("alredysalondetail", alredysalondetail);
        //query
        await SalonUser.update(
          {
            details: `${details}`,
          },
          {
            where: {
              id: `${user_id}`,
            },
          }
        );
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_salondetail(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id } = body;
        //query
        let salondetail = await SalonUser.findAll({
          include: [
            {
              model: SalonBusinessHours,
              as: "opening_hours",
              attributes: ["day", "working_start_time", "working_end_time"],
            },
          ],
          attributes: [
            "id",
            "salon_name",
            "profile_pic",
            "salon_type",
            "service_gender",
            "address",
            "details",
            "ratings",
            "counts",
          ],

          where: {
            id: `${salon_id}`,
          },
        });
        let profile = profile_pic(salondetail[0].dataValues.profile_pic);

        let i;
        let DAY = [];
        let hour = [];
        //loop
        for (let i = 1; i <= 7; i++) {
          DAY.push({ day: i });

          //query
          const busshour = await SalonBusinessHours.findOne({
            where: { salon_id: `${user_id}`, day: i },
          });
          if (busshour) {
            hour.push({
              day: busshour.day,
              working_start_time: busshour.working_start_time,
              working_end_time: busshour.working_end_time,
              is_close: 0,
            });
          } else {
            hour.push({
              day: i,
              working_start_time: "Closed",
              working_end_time: " ",
              is_close: 1,
            });
          }
        }
        //query
        let imgesvideo = await PhotosAndVideos.findAll({
          attributes: ["id", "salon_id", "images_videos", "is_image_video"],
          where: {
            salon_id: `${salon_id}`,
          },
        });
        console.log(imgesvideo[0].salon_id);

        let arrayy = [];
        //loop
        for (let i in imgesvideo) {
          if (!arrayy.includes(imgesvideo[i].id)) {
            arrayy.push({
              id: imgesvideo[i].id,
              salon_id: imgesvideo[i].salon_id,
              is_image_video: imgesvideo[i].is_image_video,
              images_videos: images_videos(imgesvideo[i].images_videos),
            });
          }
        }

        let saloninfo = await SalonUser.findAll({
          attributes: [
            "id",
            "salon_type",
            "profile_pic",
            "salon_name",
            "address",
            "counts",
            "ratings",
          ],
          include: [
            {
              model: UserFavouriteSalons,
              as: "favsalon",
              attributes: ["id", "is_favourite"],
              where: {
                users_id: `${user_id}`,
              },
            },
          ],

          where: {
            id: `${salon_id}`,
          },
        });
        console.log("saloninfo", saloninfo);

        let array1 = [];
        if (saloninfo.length > 0) {
          array1.push({
            is_favourite: saloninfo[0].favsalon[0].is_favourite,
            id: salondetail[0].dataValues.id,
            salon_name: salondetail[0].dataValues.salon_name,
            profile_pic: salondetail[0].dataValues.profile_pic,
            salon_type: salondetail[0].dataValues.salon_type,
            service_gender: salondetail[0].dataValues.service_gender,
            address: salondetail[0].dataValues.address,
            details: salondetail[0].dataValues.details,
            ratings: salondetail[0].dataValues.ratings,
            counts: salondetail[0].dataValues.counts,
            profile_pic: profile,
            opening_hours: hour,
            gallery: arrayy,
          });
        } else {
          array1.push({
            is_favourite: null,
            id: salondetail[0].dataValues.id,
            salon_name: salondetail[0].dataValues.salon_name,
            profile_pic: salondetail[0].dataValues.profile_pic,
            salon_type: salondetail[0].dataValues.salon_type,
            service_gender: salondetail[0].dataValues.service_gender,
            address: salondetail[0].dataValues.address,
            details: salondetail[0].dataValues.details,
            ratings: salondetail[0].dataValues.ratings,
            counts: salondetail[0].dataValues.counts,
            profile_pic: profile,
            opening_hours: hour,
            gallery: arrayy,
          });
        }
        return resolve({ salon_detail: array1 });
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_imagevideo(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, images_videos, id } = body;

        const result = await add_imagevideo.validateAsync(body);
        images_videos.forEach(function (value, i) {
          if (images_videos[i].match(/\.(png|jpg|jpeg)$/)) {
            let image = PhotosAndVideos.create({
              images_videos: `${images_videos[i]}`,
              salon_id: `${user_id}`,
              is_image_video: 1,
            });
          } else {
            let video = PhotosAndVideos.create({
              images_videos: `${images_videos[i]}`,
              salon_id: `${user_id}`,
              is_image_video: 2,
            });
          }
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_imagevideo(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, size } = body;
        let getPagination = (page, size) => {
          let limit = size ? +size : 3;
          let offset = page ? (page - 1) * limit : 0;

          return { limit, offset };
        };

        let getPagingData = (data, page, limit) => {
          let { count: totalrecords, rows: PhotosAndVideos } = data;

          return { totalrecords, PhotosAndVideos };
        };
        let imgvid = db.PhotosAndVideos;
        let { limit, offset } = getPagination(page, size);
        await imgvid
          .findAndCountAll({
            where: {
              salon_id: `${user_id}`,
            },
            limit,
            offset,
          })
          // console.log(a)
          .then((data) => {
            const response = getPagingData(data, page, limit);
            return resolve(response);
          })
          .catch((err) => console.log(err));

        // return resolve({ imgvid: imgvid });
      } catch (err) {
        return reject(err);
      }
    });
  }

  edit_imagevideo(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, images_videos, id } = body;
        console.log("images_", images_videos);
        const result = await edit_imagevideo.validateAsync(body);
        let imgvid = await PhotosAndVideos.findOne({
          where: {
            [Op.and]: [{ id: `${id}` }, { salon_id: `${user_id}` }],
          },
        });
        console.log("images_", imgvid);
        if (imgvid) {
          await PhotosAndVideos.update(
            {
              images_videos: `${images_videos}`,
            },
            {
              where: {
                id: `${id}`,
                salon_id: `${user_id}`,
              },
            }
          );
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  delete_imagevideo(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, id } = body;

        const result = await deleteimagevideo.validateAsync(body);
        //query
        let imgvid = await PhotosAndVideos.findOne({
          where: {
            [Op.and]: [{ id: `${id}` }, { salon_id: `${user_id}` }],
          },
        });
        console.log(imgvid.dataValues.images_videos);
        if (!imgvid) {
          let error = new Error("imageorvideo alredy delete");
        } else {
          await PhotosAndVideos.destroy({
            where: {
              [Op.and]: [{ id: `${id}` }, { salon_id: `${user_id}` }],
            },
          });

          fs.unlinkSync(config.directoryPath + imgvid.dataValues.images_videos);
        }
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_salons_detail(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { id, user_id } = body;
        const result = await listsalonsdetail.validateAsync(body);
        //query
        let favourite = await UserFavouriteSalons.findAll({
          attributes: ["is_favourite"],
          where: {
            [Op.and]: [{ users_id: `${user_id}`, salon_id: `${id}` }],
          },
        });

        //query
        let salondetail = await SalonUser.findAll({
          include: [
            {
              model: SalonBusinessHours,
              as: "opening_hours",
              attributes: ["day", "working_start_time", "working_end_time"],
            },
          ],
          attributes: [
            "id",
            "salon_name",
            "profile_pic",
            "salon_type",
            "service_gender",
            "address",
            "details",
            "ratings",
            "counts",
          ],

          where: {
            id: `${id}`,
            user_type: 2,
          },
        });
        let profile = profile_pic(salondetail[0].dataValues.profile_pic);

        let i;
        let DAY = [];
        let hour = [];
        //loop
        for (let i = 1; i <= 7; i++) {
          DAY.push({ day: i });

          //query
          const busshour = await SalonBusinessHours.findOne({
            where: { salon_id: `${id}`, day: i },
          });
          if (busshour) {
            hour.push({
              day: busshour.day,
              working_start_time: busshour.working_start_time,
              working_end_time: busshour.working_end_time,
              is_close: 0,
            });
          } else {
            hour.push({
              day: i,
              working_start_time: "Closed",
              working_end_time: " ",
              is_close: 1,
            });
          }
        }
        //query
        let imgesvideo = await PhotosAndVideos.findAll({
          attributes: ["id", "salon_id", "images_videos", "is_image_video"],
          where: {
            salon_id: `${id}`,
          },
        });

        let arrayy = [];
        //loop
        for (let i in imgesvideo) {
          if (!arrayy.includes(imgesvideo[i].id)) {
            arrayy.push({
              id: imgesvideo[i].id,
              salon_id: imgesvideo[i].salon_id,
              is_image_video: imgesvideo[i].is_image_video,
              images_videos: images_videos(imgesvideo[i].images_videos),
            });
          }
        }

        let array1 = {
          id: salondetail[0].dataValues.id,
          salon_name: salondetail[0].dataValues.salon_name,
          profile_pic: salondetail[0].dataValues.profile_pic,
          salon_type: salondetail[0].dataValues.salon_type,
          service_gender: salondetail[0].dataValues.service_gender,
          address: salondetail[0].dataValues.address,
          details: salondetail[0].dataValues.details,
          ratings: salondetail[0].dataValues.ratings,
          counts: salondetail[0].dataValues.counts,
          profile_pic: profile,
          is_favourite: favourite[0].dataValues.is_favourite,
          opening_hours: hour,
          gallery: arrayy,
        };

        return resolve({ salon_detail: array1 });
      } catch (err) {
        return reject(err);
      }
    });
  }

  referal_code(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { id } = body;
        const result = await refercode.validateAsync(body);
        let salonuser = await SalonUser.findOne({
          attributes: ["email"],
          where: {
            id: `${id}`,
          },
        });
        console.log("str", salonuser.dataValues.email);

        let string = `${salonuser.dataValues.email}`;
        let str = string.substring(0, 4);
        console.log("str", str);
        let referalcode = referCode.custom("uppercase", 3, 5, `${str}`);

        console.log("str", referalcode);
        await SalonUser.update(
          {
            referral_code: `${referalcode}`,
          },
          {
            where: {
              id: `${id}`,
            },
          }
        );

        // user[0].otp = otp;
        // await SalonUser.create({
        //   where: {
        //     referalcode: {referalcode},
        //   },
        // });
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_order(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          salon_service_id,
          user_id,
          total,
          salon_id,
          barber_id,
          order_date,
          order_time,
          is_walkin_user_appointment,
          walkin_user_name,
          walkin_user_country_code,
          walkin_user_number,
        } = body;
        console.log("arr", body.salon_id);

        // const result = await add_order.validateAsync(body);
        if (is_walkin_user_appointment == 0) {
          let check_date = await OrderOfUser.findAll({
            where: {
              order_date: `${order_date}`,
              order_time: `${order_time}`,
            },
          });
          console.log(check_date);
          if (check_date.length == 0) {
            let order = await OrderOfUser.create({
              salon_id: `${salon_id}`,
              barber_id: `${barber_id}`,
              order_date: `${order_date}`,
              order_time: `${order_time}`,
              total: `${total}`,
              users_id: `${user_id}`,
              is_cancel: 0,
            });
            console.log(order.dataValues.id);
            let user = await OrderService.findAll({
              attributes: ["order_of_users_id"],
              where: {
                order_of_users_id: `${order.dataValues.id}`,
              },
            });
            if (user.length == 0) {
              salon_service_id.forEach(async (value, i) => {
                console.log("salon_service_id", value);

                await OrderService.create({
                  order_of_users_id: `${order.dataValues.id}`,
                  users_id: `${user_id}`,
                  salon_service_id: `${value}`,
                });
              });

              let token = await PushDevice.findAll({
                attributes: ["device_type", "device_token"],
                where: {
                  user_id: `${user_id}`,
                },
              });

              let Token = await PushDevice.findAll({
                attributes: ["device_type", "device_token"],
                where: {
                  user_id: `${salon_id}`,
                },
              });

              if (token.length > 0) {
                let title = "Salon Booking",
                  body = "your salon appointment booking booked successfully";
                let notification = {
                  title,
                  body,
                };

                token = token[0].device_token;

                let send_notification = await firebase.sendFCMPushNotification(
                  token,
                  notification,
                  {
                    name: "test",
                  }
                );

                let Data = {
                  send_id: `${salon_id}`,
                  accept_id: `${user_id}`,
                  title: `${title}`,
                  body: `${body}`,
                };

                let Notification = await UserSalonNotification.create(Data);
              }

              if (Token.length > 0) {
                let title = "Salon Booking",
                  body = "your upcoming appointment order by user";
                let notification = {
                  title,
                  body,
                };

                token = Token[0].device_token;

                let send_notification = await firebase.sendFCMPushNotification(
                  token,
                  notification,
                  {
                    name: "test",
                  }
                );

                let Data = {
                  send_id: `${user_id}`,
                  accept_id: `${salon_id}`,
                  title: `${title}`,
                  body: `${body}`,
                };

                let Notification = await UserSalonNotification.create(Data);
              }
            }
          } else {
            let error = new Error(
              "please select another time. this time was also appointed"
            );
            error.code = 400;
            return reject(error);
          }
        } else {
          let check_date = await OrderOfUser.findAll({
            where: {
              order_date: `${order_date}`,
              order_time: `${order_time}`,
            },
          });
          console.log(check_date);
          if (check_date.length == 0) {
            let order = await OrderOfUser.create({
              salon_id: `${user_id}`,
              total: `${total}`,
              is_cancel: 0,
              order_date: `${order_date}`,
              order_time: `${order_time}`,
              is_walkin_user_appointment: 1,
              walkin_user_name: `${walkin_user_name}`,
              walkin_user_country_code: `${walkin_user_country_code}`,
              walkin_user_number: `${walkin_user_number}`,
            });

            console.log(order.dataValues.id);
            let user = await OrderService.findAll({
              attributes: ["order_of_users_id"],
              where: {
                order_of_users_id: `${order.dataValues.id}`,
              },
            });
            if (user.length == 0) {
              salon_service_id.forEach(async (value, i) => {
                console.log("salon_service_id", value);

                await OrderService.create({
                  order_of_users_id: `${order.dataValues.id}`,
                  users_id: `${user_id}`,
                  salon_service_id: `${value}`,
                });
              });

              let token = await PushDevice.findAll({
                attributes: ["device_type", "device_token"],
                where: {
                  user_id: `${user_id}`,
                },
              });

              let Token = await PushDevice.findAll({
                attributes: ["device_type", "device_token"],
                where: {
                  user_id: `${salon_id}`,
                },
              });

              if (token.length > 0) {
                let title = "Salon Booking",
                  body = "your salon appointment booking booked successfully";
                let notification = {
                  title,
                  body,
                };

                token = token[0].device_token;

                let send_notification = await firebase.sendFCMPushNotification(
                  token,
                  notification,
                  {
                    name: "test",
                  }
                );

                let Data = {
                  send_id: `${salon_id}`,
                  accept_id: `${user_id}`,
                  title: `${title}`,
                  body: `${body}`,
                };

                let Notification = await UserSalonNotification.create(Data);
              }

              if (Token.length > 0) {
                let title = "Salon Booking",
                  body = "your upcoming appointment order by user";
                let notification = {
                  title,
                  body,
                };

                token = Token[0].device_token;

                let send_notification = await firebase.sendFCMPushNotification(
                  token,
                  notification,
                  {
                    name: "test",
                  }
                );

                let Data = {
                  send_id: `${user_id}`,
                  accept_id: `${salon_id}`,
                  title: `${title}`,
                  body: `${body}`,
                };

                let Notification = await UserSalonNotification.create(Data);
              }
            }
          } else {
            let error = new Error(
              "please select another time. this time was also appointed"
            );
            error.code = 400;
            return reject(error);
          }
        }
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  order_detail(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id } = body;

        let userorder = await OrderOfUser.findAll({
          include: [{ model: Service, as: "service", attributes: ["price"] }],

          where: {
            salon_id: `${salon_id}`,
            users_id: `${user_id}`,
          },
        });
        let array_price = [];
        for (let m in userorder) {
          if (!array_price.includes(userorder[m].dataValues.id)) {
            array_price.push(userorder[m].service.dataValues.price);
          }
        }
        console.log("list", array_price);
        await OrderOfUser.update(
          { total: array_price.reduce((a, b) => a + b, 0) },
          {
            where: {
              salon_id: `${salon_id}`,
              users_id: `${user_id}`,
            },
          }
        );
        let user = await OrderOfUser.findAll({
          include: [
            {
              model: Service,
              as: "service",
              attributes: ["name", "price"],
            },
          ],
          where: {
            salon_id: `${salon_id}`,
            users_id: `${user_id}`,
          },
        });

        let orderlistarray = [];
        for (let t in user) {
          if (!orderlistarray.includes(user[t].dataValues.id)) {
            orderlistarray.push({
              name: user[t].service.dataValues.name,
              price: user[t].service.dataValues.price,
            });
          }
        }

        return resolve({
          userorder: {
            salon_id: user[0].dataValues.salon_id,
            total: user[0].dataValues.total,
            staff_business_hours_id: user[0].dataValues.staff_business_hours_id,
            salon_category_id: user[0].dataValues.salon_category_id,
          },
          salonservice: orderlistarray,
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  date_time(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { date, month, year, id, user_id, barber_id, salon_id } = body;
        // attributes:["walkin_name","walkin_country_code","walkin_number"],
        let userorder = await OrderOfUser.findAll({
          attributes: [
            "walkin_user_name",
            "walkin_user_country_code",
            "walkin_user_number",
          ],
          include: [
            {
              model: SalonUser,
              as: "salon",
              attributes: [],
            },

            {
              model: Service,
              as: "service",
              attributes: ["id", "name", "price"],
              through: {
                attributes: [],
              },
            },
          ],
          where: {
            id: `${id}`,
          },
        });

        console.log("userorder", userorder);
        const result = await date_time.validateAsync(body);
        let formdate = `${date}`;

        let formatdate = new Date(`${date}`);
        let day = formatdate.getDay();

        let staff = await StaffBusinessHours.findAll({
          attributes: [
            "working_start_time",
            "working_end_time",
            "break_start_time",
            "break_end_time",
          ],
          where: {
            salon_id: `${salon_id}`,
            barber_id: `${barber_id}`,
            date: `${formdate}`,
          },
        });
        console.log(staff);
        if (staff.length == 0) {
          let staff = await StaffBusinessHours.findAll({
            attributes: [
              "working_start_time",
              "working_end_time",
              "break_start_time",
              "break_end_time",
            ],
            where: {
              salon_id: `${salon_id}`,
              barber_id: `${barber_id}`,
              day: `${day}`,
            },
          });

          function addMinutes(dat, minutes) {
            let mom = moment(dat, "HH:mm A")
              .add(minutes, "minutes")
              .format("HH:mm A");
            return mom;
          }
          let dat = moment(
            staff[0].dataValues.working_start_time,
            "HH:mm A"
          ).format("HH:mm A");

          let enddat = moment(
            staff[0].dataValues.working_end_time,
            "HH:mm A"
          ).format("HH:mm A");

          let breaktm = moment(
            staff[0].dataValues.break_start_time,
            "HH:mm A"
          ).format("HH:mm A");

          let breakfn = moment(
            staff[0].dataValues.break_end_time,
            "HH:mm A"
          ).format("HH:mm A");

          let array = [];
          while (dat <= enddat) {
            if (!array.includes(dat)) {
              array.push(dat);
            }
            dat = addMinutes(dat, 10);
          }
          console.log(array);

          let array2 = [];

          for (let i in array) {
            if (breaktm > array[i]) {
              array2.push(array[i]);
            }
          }
          for (let j in array) {
            if (breakfn <= array[j]) {
              array2.push(array[j]);
            }
          }

          return resolve(array2);
        }

        function addMinutes(dat, minutes) {
          let mom = moment(dat, "HH:mm A")
            .add(minutes, "minutes")
            .format("HH:mm A");
          return mom;
        }
        let dat = moment(
          staff[0].dataValues.working_start_time,
          "HH:mm A"
        ).format("HH:mm A");

        let enddat = moment(
          staff[0].dataValues.working_end_time,
          "HH:mm A"
        ).format("HH:mm A");

        let breaktm = moment(
          staff[0].dataValues.break_start_time,
          "HH:mm A"
        ).format("HH:mm A");

        let breakfn = moment(
          staff[0].dataValues.break_end_time,
          "HH:mm A"
        ).format("HH:mm A");

        let array = [];
        while (dat <= enddat) {
          if (!array.includes(dat)) {
            array.push(dat);
          }
          dat = addMinutes(dat, 10);
        }
        console.log(array);

        let array2 = [];

        for (let i in array) {
          if (breaktm > array[i]) {
            array2.push(array[i]);
          }
        }
        for (let j in array) {
          if (breakfn <= array[j]) {
            array2.push(array[j]);
          }
        }

        return resolve({ userorder: userorder, time: array2 });
      } catch (err) {
        return reject(err);
      }
    });
  }

  order_cancelled_by_user_salon(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, id } = body;
        const result = await cancelled_order.validateAsync(body);
        let userType = await SalonUser.findAll({
          where: {
            id: `${user_id}`,
          },
        });
        console.log(userType[0].user_type);

        let userorder = await OrderOfUser.findOne({
          where: {
            salon_id: `${user_id}`,
            is_walkin_user_appointment: 1,
            id: `${id}`,
          },
        });
        console.log("user", userorder.dataValues.is_walkin_user_appointment);

        if (userorder.dataValues.is_walkin_user_appointment == 1) {
          let user_walkin = await OrderOfUser.findAll({
            where: {
              salon_id: `${user_id}`,
              is_walkin_user_appointment: 1,
              id: `${id}`,
            },
          });

          if (user_walkin.length > 0) {
            await OrderOfUser.update(
              {
                is_cancel: 1,
                cancelled_by: 2,
                order_status: 2,
              },
              {
                where: {
                  salon_id: `${user_id}`,
                  id: `${id}`,
                  is_cancel: 0,
                },
              }
            );
          }
        } else {
          if (userType[0].user_type == 1) {
            let userorder = await OrderOfUser.findAll({
              where: {
                users_id: `${user_id}`,
                id: `${id}`,
              },
            });
            console.log(userorder.length);
            if (userorder.length > 0) {
              await OrderOfUser.update(
                {
                  is_cancel: 1,
                  cancelled_by: 1,
                  order_status: 2,
                },
                {
                  where: {
                    users_id: `${user_id}`,
                    id: `${id}`,
                    is_cancel: 0,
                  },
                }
              );
            }

            let token = await PushDevice.findAll({
              attributes: ["device_type", "device_token"],
              where: {
                user_id: `${userorder[0].dataValues.salon_id}`,
              },
            });
            console.log(token[0].device_token);

            if (token.length > 0) {
              let title = "Salon Booking ",
                body = "appointment cancelled successfully";
              let notification = {
                title,
                body,
              };

              console.log(token[0].device_token);

              token = token[0].device_token;

              let send_notification = await firebase.sendFCMPushNotification(
                token,
                notification,
                {
                  name: "test",
                }
              );

              let Data = {
                send_id: `${user_id}`,
                accept_id: `${userorder[0].dataValues.salon_id}`,
                title: `${title}`,
                body: `${body}`,
              };

              let Notification = await UserSalonNotification.create(Data);
            }
          } else {
            let userorder = await OrderOfUser.findAll({
              where: {
                salon_id: `${user_id}`,
                id: `${id}`,
              },
            });
            console.log(userorder[0].dataValues.users_id);
            if (userorder.length > 0) {
              await OrderOfUser.update(
                {
                  is_cancel: 1,
                  cancelled_by: 2,
                  order_status: 2,
                },
                {
                  where: {
                    salon_id: `${user_id}`,
                    id: `${id}`,
                    is_cancel: 0,
                  },
                }
              );
            }
            let token = await PushDevice.findAll({
              attributes: ["device_type", "device_token"],
              where: {
                user_id: `${userorder[0].dataValues.users_id}`,
              },
            });
            console.log(token[0].device_token);

            if (token.length > 0) {
              let title = "Salon Booking",
                body = "appointment cancelled successfully";
              let notification = {
                title,
                body,
              };

              console.log(token[0].device_token);

              token = token[0].device_token;

              let send_notification = await firebase.sendFCMPushNotification(
                token,
                notification,
                {
                  name: "test",
                }
              );

              let Data = {
                send_id: `${user_id}`,
                accept_id: `${userorder[0].dataValues.users_id}`,
                title: `${title}`,
                body: `${body}`,
              };

              let Notification = await UserSalonNotification.create(Data);
            }
          }
        }

        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  user_appointment(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, order_status, size, page } = body;
        const result = await appointment.validateAsync(body);
        let order_appointment = await OrderOfUser.findAll({
          attributes: [
            "id",
            "order_date",
            "order_time",
            "total",
            "order_status",
          ],
          include: [
            {
              model: SalonUser,
              as: "salon",
              attributes: [
                "id",
                "salon_name",
                "salon_type",
                "profile_pic",
                "ratings",
              ],
            },
            { model: SalonStaff, as: "barber", attributes: ["name"] },

            {
              model: Service,
              as: "service",
              attributes: ["id", "name", "price"],
              through: {
                attributes: [],
              },
            },
          ],
          where: {
            users_id: ` ${user_id}`,
            order_status: `${order_status}`,
          },
        });

        console.log(order_appointment[0].dataValues.order_status);

        let array_appoint = [];
        for (let i in order_appointment) {
          if (!array_appoint.includes(order_appointment[0].dataValues.id)) {
            array_appoint.push({
              id: order_appointment[i].dataValues.salon.dataValues.id,
              salon_name:
                order_appointment[i].dataValues.salon.dataValues.salon_name,
              salon_type:
                order_appointment[i].dataValues.salon.dataValues.salon_type,
              profile_pic: profile_pic(
                order_appointment[i].dataValues.salon.dataValues.profile_pic
              ),
              ratings: order_appointment[i].dataValues.salon.dataValues.ratings,
              order_date: order_appointment[i].dataValues.order_date,
              order_time: order_appointment[i].dataValues.order_time,
              total: order_appointment[i].dataValues.total,
              barber_name: order_appointment[i].dataValues.barber.name,
              service: order_appointment[i].dataValues.service,
            });
          }
        }

        //1
        let array_isfav_id = [];
        //2
        for (let g in array_appoint) {
          if (!array_isfav_id.includes(array_appoint[g].id)) {
            array_isfav_id.push(array_appoint[g].id);
          }
        }

        //3
        let salon_info = await SalonUser.findAll({
          attributes: [
            "id",
            "salon_type",
            "profile_pic",
            "salon_name",
            "address",
            "counts",
            "ratings",
          ],
          include: [
            {
              model: UserFavouriteSalons,
              as: "favsalon",
              attributes: ["id", "is_favourite"],
              where: {
                salon_id: array_isfav_id,
                users_id: `${user_id}`,
              },
            },
          ],

          where: {
            id: array_isfav_id,
          },
        });

        //4
        let array_result = [];

        for (let d in salon_info) {
          array_result.push({
            id: array_appoint[d].id,
            salon_name: array_appoint[d].salon_name,
            salon_type: array_appoint[d].salon_type,
            profile_pic: profile_pic(array_appoint[d].profile_pic),
            ratings: array_appoint[d].ratings,
            order_date: array_appoint[d].order_date,
            order_time: array_appoint[d].order_time,
            total: array_appoint[d].total,
            barber_name: array_appoint[d].name,
            service: array_appoint[d].service,
            is_favourite: salon_info[d].favsalon[0].is_favourite,
          });
        }

        //5
        let arr_isfav = [];
        for (let g in array_result) {
          if (!arr_isfav.includes(array_result[g].id)) {
            arr_isfav.push(array_result[g].id);
          }
        }

        //6
        let salon_user = await SalonUser.findAll({
          attributes: [
            "id",
            "salon_type",
            "profile_pic",
            "salon_name",
            "address",
            "counts",
            "ratings",
          ],

          where: {
            id: array_isfav_id,
          },
        });

        //7
        for (let e in array_appoint) {
          console.log("array_", array_appoint);
          if (!arr_isfav.includes(array_appoint[e].id)) {
            {
              array_result.push({
                id: array_appoint[e].id,
                salon_name: array_appoint[e].salon_name,
                salon_type: array_appoint[e].salon_type,
                profile_pic: profile_pic(array_appoint[e].profile_pic),
                ratings: array_appoint[e].ratings,
                order_date: array_appoint[e].order_date,
                order_time: array_appoint[e].order_time,
                total: array_appoint[e].total,
                barber_name: array_appoint[e].name,
                service: array_appoint[e].service,
                is_favourite: null,
              });
            }
          }
        }

        return resolve(paginatedata(array_result, size, page));
      } catch (err) {
        return reject(err);
      }
    });
  }

  salon_appointment(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, order_status, start_date, end_date, page, size } = body;
        const result = await appointment.validateAsync(body);

        let orderappointment = await OrderOfUser.findAll({
          include: [
            {
              model: User,
              as: "user",
              attributes: ["name"],
            },
            { model: SalonStaff, as: "barber", attributes: ["name"] },

            {
              model: Service,
              as: "service",
              attributes: ["id", "name", "price", "time"],
              through: {
                attributes: [],
              },
            },
          ],
          where: {
            salon_id: `${user_id}`,
            is_walkin_user_appointment: null,
            order_status: `${order_status}`,
          },
        });

        console.log("walkin", orderappointment);
        let array_ = [];
        for (let j in orderappointment) {
          array_.push({
            id: orderappointment[j].dataValues.id,
          });
        }

        let array_appoint = [];
        for (let i in orderappointment) {
          console.log("o", orderappointment[i].dataValues.id);
          if (!array_appoint.includes(orderappointment[i].dataValues.id)) {
            console.log(i);
            array_appoint.push({
              id: orderappointment[i].dataValues.id,
              name: orderappointment[i].dataValues.user.dataValues.name,
              order_date: orderappointment[i].dataValues.order_date,
              order_time: orderappointment[i].dataValues.order_time,
              total: orderappointment[i].dataValues.total,
              barber_name: orderappointment[i].dataValues.barber.name,
              service: orderappointment[i].dataValues.service,
            });
          }
        }

        var startDate = moment(`${start_date}`).format("MM-DD-YYYY");
        var endDate = moment(`${end_date}`).format("MM-DD-YYYY");

        var appointmentData = array_appoint.filter((a) => {
          let date = moment(a.order_date).format("MM-DD-YYYY");
          console.log(date >= startDate && date <= endDate);
          return date >= startDate && date <= endDate;
        });
        console.log("r", appointmentData);

        let order_walkin = await OrderOfUser.findAll({
          include: [
            {
              model: Service,
              as: "service",
              attributes: ["id", "name", "price", "time"],
              through: {
                attributes: [],
              },
            },
          ],
          where: {
            salon_id: `${user_id}`,
            is_walkin_user_appointment: 1,
            order_status: `${order_status}`,
          },
        });
        console.log("walkin", order_walkin);
        // let array_walkin_user = [];
        for (let i in order_walkin) {
          if (!array_.includes(order_walkin[i].dataValues.id)) {
            console.log("walkin", order_walkin[i].dataValues.walkin_user_name);
            appointmentData.push({
              id: order_walkin[i].dataValues.id,
              name: order_walkin[i].dataValues.walkin_user_name,
              order_date: order_walkin[i].dataValues.order_date,
              order_time: order_walkin[i].dataValues.order_time,
              total: order_walkin[i].dataValues.total,
              barber_name: null,
              service: order_walkin[i].dataValues.service,
            });
          }
        }

        return resolve({ appointmentData: appointmentData });
      } catch (err) {
        return reject(err);
      }
    });
  }

  user_appointment_completed(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { accept_id, user_id, id } = body;
        // order time:11:10 AM
        await completed_order.validateAsync(body);
        let order = await OrderOfUser.findAll({
          where: {
            id: `${id}`,
            is_walkin_user_appointment: null,
          },
        });
        if (order.length > 0) {
          await OrderOfUser.update(
            {
              order_status: 1,
            },
            {
              where: {
                id: `${id}`,
                is_cancel: 0,
              },
            }
          );

          let token = await PushDevice.findAll({
            attributes: ["device_type", "device_token"],
            where: {
              user_id: `${order[0].dataValues.users_id}`,
            },
          });
          console.log(token[0].device_token);

          if (token.length > 0) {
            let title = "Salon Booking",
              body = "appointment completed successfully";
            let notification = {
              title,
              body,
            };

            console.log(token[0].device_token);

            token = token[0].device_token;

            let send_notification = await firebase.sendFCMPushNotification(
              token,
              notification,
              {
                name: "test",
              }
            );

            let Data = {
              send_id: `${user_id}`,
              accept_id: `${accept_id}`,
              title: `${title}`,
              body: `${body}`,
            };

            let Notification = await UserSalonNotification.create(Data);
          }
        }
        let order_walkin = await OrderOfUser.findAll({
          where: {
            id: `${id}`,
            is_walkin_user_appointment: 1,
          },
        });
        if (order_walkin.length > 0) {
          await OrderOfUser.update(
            {
              order_status: 1,
            },
            {
              where: {
                id: `${id}`,
                is_cancel: 0,
              },
            }
          );
        }

        console.log("order_completed");
      } catch (err) {
        return reject(err);
      }
    });
  }

  user_device_relation(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, device_token, device_type, device_id } = body;

        const whereQuery = {
          user_id: `${user_id}`,
          device_type: `${device_type}`,
          device_id: `${device_id}`,
        };
        const result = await user_device_relation.validateAsync(body);
        //1
        await PushDevice.findOrCreate({
          where: whereQuery,
        });

        //2

        await PushDevice.update(
          { device_token: `${device_token}` },
          { where: whereQuery }
        );

        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  salon_appointment_calender(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, order_date, page, size } = body;
        const result = await salon_appointment_calender.validateAsync(body);

        let orderappointment = await OrderOfUser.findAndCountAll({
          attributes: ["id", "order_date", "order_time", "total"],
          include: [
            {
              model: User,
              as: "user",
              attributes: ["name"],
            },
            { model: SalonStaff, as: "barber", attributes: ["name"] },

            {
              model: Service,
              as: "service",
              attributes: ["id", "name", "price", "time"],
              through: {
                attributes: [],
              },
            },
          ],
          where: {
            [Op.and]: [
              { salon_id: `${user_id}`, order_status: 0 },

              { order_date: `${order_date}` },
            ],
          },
        });

        let total_appointment = await OrderOfUser.findAndCountAll({
          attributes: ["id", "order_date", "order_time", "total"],
          include: [
            {
              model: User,
              as: "user",
              attributes: ["name"],
            },
          ],
          where: {
            [Op.and]: [
              { salon_id: `${user_id}`, order_status: 0 },

              { order_date: `${order_date}` },
            ],
          },
        });
        console.log(total_appointment.count);
        let array_appoint = [{ total_appintment: total_appointment.count }];
        for (let i in orderappointment.rows) {
          console.log(orderappointment.rows[1].dataValues.id);
          if (!array_appoint.includes(orderappointment.rows[i].dataValues.id)) {
            array_appoint.push({
              name: orderappointment.rows[i].dataValues.user.dataValues.name,
              order_date: orderappointment.rows[i].dataValues.order_date,
              order_time: orderappointment.rows[i].dataValues.order_time,
              total: orderappointment.rows[i].dataValues.total,
              barber_name: orderappointment.rows[i].dataValues.barber.name,
              service: orderappointment.rows[i].dataValues.service,
            });
          }
        }

        return resolve(paginatedata(array_appoint, size, page));
      } catch (err) {
        return reject(err);
      }
    });
  }

  barber_appointment_calender(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, order_date, barber_id, page, size } = body;
        const result = await barber_appointment_calender.validateAsync(body);

        let orderappointment = await OrderOfUser.findAndCountAll({
          attributes: ["id", "order_date", "order_time", "total"],
          include: [
            {
              model: User,
              as: "user",
              attributes: ["name"],
            },
            { model: SalonStaff, as: "barber", attributes: ["name"] },

            {
              model: Service,
              as: "service",
              attributes: ["id", "name", "price", "time"],
              through: {
                attributes: [],
              },
            },
          ],
          where: {
            [Op.and]: [
              {
                salon_id: `${user_id}`,
                barber_id: `${barber_id}`,
                order_status: 0,
              },

              { order_date: `${order_date}` },
            ],
          },
        });

        let total_appointment = await OrderOfUser.findAndCountAll({
          attributes: ["id", "order_date", "order_time", "total"],
          include: [
            {
              model: User,
              as: "user",
              attributes: ["name"],
            },
          ],
          where: {
            [Op.and]: [
              {
                salon_id: `${user_id}`,
                barber_id: `${barber_id}`,
                order_status: 0,
              },

              { order_date: `${order_date}` },
            ],
          },
        });
        console.log(total_appointment.count);
        let array_appoint = [{ total_appintment: total_appointment.count }];
        for (let i in orderappointment.rows) {
          console.log(orderappointment.rows[1].dataValues.id);
          if (!array_appoint.includes(orderappointment.rows[i].dataValues.id)) {
            array_appoint.push({
              name: orderappointment.rows[i].dataValues.user.dataValues.name,
              order_date: orderappointment.rows[i].dataValues.order_date,
              order_time: orderappointment.rows[i].dataValues.order_time,
              total: orderappointment.rows[i].dataValues.total,
              barber_name: orderappointment.rows[i].dataValues.barber.name,
              service: orderappointment.rows[i].dataValues.service,
            });
          }
        }

        return resolve(paginatedata(array_appoint, size, page));
      } catch (err) {
        return reject(err);
      }
    });
  }

  send_notification(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id } = body;

        let token = await PushDevice.findAll({
          attributes: ["device_type", "device_token"],
          where: {
            user_id: `${salon_id}`,
          },
        });
        console.log(token[0].device_token);
        console.log(token.length);
        if (token.length > 0) {
          let title = "test",
            body = "test";
          let notification = {
            title,
            body,
          };
          let data = 1;

          token = token[0].device_token;

          await firebase.sendFCMPushNotification(
            token,
            notification,
            {
              name: "test",
            },
            data
          );
        }

        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  add_notification(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { accept_id, user_id, Title, Body } = body;

        let token = await PushDevice.findAll({
          attributes: ["device_type", "device_token"],
          where: {
            user_id: `${accept_id}`,
          },
        });
        console.log(token[0].device_token);
        console.log(token.length);
        if (token.length > 0) {
          let notification = {
            title: `${Title}`,
            body: `${Body}`,
          };
          let data = 1;

          token = token[0].device_token;

          await firebase.sendFCMPushNotification(
            token,
            notification,
            {
              name: "test",
            },
            data
          );

          let Data = {
            send_id: `${user_id}`,
            accept_id: `${accept_id}`,
            title: `${Title}`,
            body: `${Body}`,
          };

          let Notification = await UserSalonNotification.create(Data);
        }

        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  list_notification(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, size } = body;

        let getPagination = (page, size) => {
          let limit = size ? +size : 3;
          let offset = page ? (page - 1) * limit : 0;

          return { limit, offset };
        };

        let getPagingData = (data, page, limit) => {
          let { count: totalrecords, rows: UserSalonNotification } = data;

          return { totalrecords, UserSalonNotification };
        };
        //query
        const user_notification = db.UserSalonNotification;
        let { limit, offset } = getPagination(page, size);

        await user_notification
          .findAndCountAll({
            attributes: ["id", "title", "body", "createdAt"],
            where: {
              accept_id: `${user_id}`,
            },
            limit,
            offset,
          })
          .then((data) => {
            const response = getPagingData(data, page, limit);
            return resolve(response);
          })
          .catch((err) => console.log(err));

        // return resolve({ user_notification: user });
      } catch (err) {
        return reject(err);
      }
    });
  }

  delete_notification(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, id } = body;

        const result = await delete_notification.validateAsync(body);

        let user_notification = await UserSalonNotification.findAll({
          where: {
            id: `${id}`,
          },
        });

        if (user_notification.length > 0) {
          let Notification = await UserSalonNotification.destroy({
            where: {
              id: `${id}`,
            },
          });
        }

        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  cron_job(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { accept_id, user_id, id } = body;

        let order = await OrderOfUser.findAll({
          where: {
            id: `${id}`,
          },
        });

        console.log(order[0].dataValues.order_time);
        console.log(order[0].dataValues.order_date);

        let orderTime = convertTimeFormat(order[0].dataValues.order_time);
        console.log("orderTime", orderTime);

        cron.schedule("* * * * *", function () {
          let objectDate = new Date();
          let day = objectDate.getDate();
          let month = objectDate.getMonth();
          let year = objectDate.getFullYear();

          if (day < 10) {
            day = "0" + day;
          }

          if (month + 1 < 10) {
            month = `0${month + 1}`;
          }

          let format_date = `${month}/${day}/${year}`;
          console.log("format_date", format_date);

          let minute = objectDate.getMinutes();
          let hour = objectDate.getHours();
          let ampm = hour >= 12 ? "PM" : "AM";

          if (minute < 10) {
            minute = "0" + minute;
          }
          if (hour < 10) {
            hour = "0" + hour;
          }

          let format_time = `${hour}:${minute} ${ampm}`;
          let convert_time_format = convertTimeFormat(format_time);
          console.log("convert_time_format", convert_time_format);

          if (
            order[0].dataValues.order_date == format_date &&
            orderTime == convert_time_format
          ) {
            OrderOfUser.update(
              {
                order_status: 1,
              },
              {
                where: {
                  id: `${id}`,
                  is_cancel: 0,
                },
              }
            );
            console.log("order_completed");
          }

          console.log("running a task every minute");
        });

        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  salon_earnings(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id,
          id,
          salon_sale,
          start_date,
          end_date,
          order_status,
          page,
          size,
        } = body;

        await salon_earnings.validateAsync(body);

        //query
        let order_appointment = await OrderOfUser.findAll({
          attributes: ["id", "order_date", "total"],
          include: [
            {
              model: User,
              as: "user",
              attributes: ["name"],
            },
          ],
          where: {
            salon_id: `${user_id}`,
            is_walkin_user_appointment: null,
            order_status: `${order_status}`,
            salon_sale: `${salon_sale}`,
          },
        });

        //query
        let order_of_users = await OrderOfUser.findAll({
          where: {
            salon_id: `${user_id}`,
            order_status: `${order_status}`,
          },
        });

        let array_total_earned = [];
        for (let m in order_of_users) {
          if (!array_total_earned.includes(order_of_users[m].dataValues.id)) {
            array_total_earned.push(order_of_users[m].dataValues.total);
          }
        }
        console.log("list", array_total_earned);
        let total_earned = array_total_earned.reduce((a, b) => a + b, 0);

        console.log(total_earned);

        let array_users_appointment = [];
        for (let m in order_appointment) {
          console.log("users", order_appointment[m].dataValues.total);
          if (
            !array_users_appointment.includes(
              order_appointment[m].dataValues.id
            )
          ) {
            array_users_appointment.push({
              id: order_appointment[m].dataValues.id,
              name: order_appointment[m].dataValues.user.dataValues.name,
              order_date: order_appointment[m].dataValues.order_date,
              total: order_appointment[m].dataValues.total,
            });
          }
        }

        //filter_date
        var startDate = moment(`${start_date}`).format("MM-DD-YYYY");
        var endDate = moment(`${end_date}`).format("MM-DD-YYYY");

        var appointmentData = array_users_appointment.filter((a) => {
          let date = moment(a.order_date).format("MM-DD-YYYY");
          console.log(date >= startDate && date <= endDate);
          return date >= startDate && date <= endDate;
        });
        console.log("r", appointmentData);

        //query
        let total_appointment = await OrderOfUser.findAndCountAll({
          where: {
            salon_id: `${user_id}`,
            order_status: `${order_status}`,
          },
        });

        console.log("salon_earning_data", {
          total_earned: total_earned,
          total_appointment: total_appointment.count,
        });

        //query
        let walkin_appointment = await OrderOfUser.findAll({
          attributes: ["id", "order_date", "total", "walkin_user_name"],

          where: {
            salon_id: `${user_id}`,
            is_walkin_user_appointment: 1,
            order_status: `${order_status}`,
            salon_sale: `${salon_sale}`,
          },
        });

        for (let i in walkin_appointment) {
          console.log(walkin_appointment[i].dataValues);
          if (!appointmentData.includes(walkin_appointment[i].dataValues.id)) {
            appointmentData.push({
              id: walkin_appointment[i].dataValues.id,
              name: walkin_appointment[i].dataValues.walkin_user_name,
              order_date: walkin_appointment[i].dataValues.order_date,

              total: walkin_appointment[i].dataValues.total,
            });
          }
        }

        return resolve({
          total_earned: total_earned,
          total_appointment: total_appointment.count,
          appointmentData: paginatedata(appointmentData, size, page),
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  salon_dashboard(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id } = body;

        let salon = await SalonUser.findAll({
          where: {
            id: `${user_id}`,
          },
        });

        console.log();
        let all_appointments_of_salon = await OrderOfUser.findAndCountAll({
          where: {
            salon_id: `${user_id}`,
          },
        });

        let total_app_users = await User.findAndCountAll({});

        return resolve({
          total_appointments: all_appointments_of_salon.count,
          total_app_users: total_app_users.count,
          profile_pic: profile_pic(salon[0].profile_pic),
          salon_name: salon[0].salon_name,
          salon_type: salon[0].salon_type,
          ratings: salon[0].ratings,
          reviews: salon[0].counts,
          salon_type: salon[0].salon_type,
          email: salon[0].email,
          country_code: salon[0].country_code,
          mobile: salon[0].mobile,
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  user_dashboard(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id } = body;

        let user = await User.findOne({
          where: {
            id: `${user_id}`,
          },
        });

        let total_apponint = await OrderOfUser.findAndCountAll({
          where: {
            users_id: `${user_id}`,
          },
        });

        let upcoming_apponint = await OrderOfUser.findAndCountAll({
          where: {
            users_id: `${user_id}`,
            order_status: 0,
          },
        });

        let completed_apponint = await OrderOfUser.findAndCountAll({
          where: {
            users_id: `${user_id}`,
            order_status: 1,
          },
        });

        let cancelled_apponint = await OrderOfUser.findAndCountAll({
          where: {
            users_id: `${user_id}`,
            order_status: 2,
          },
        });

        console.log();
        return resolve({
          profile_pic: profile_pic(user.profile_pic),
          name: user.name,
          email: user.email,
          country_code: user.country_code,
          mobile: user.mobile,
          total_apponint: total_apponint.count,
          upcoming_apponint: upcoming_apponint.count,
          completed_apponint: completed_apponint.count,
          cancelled_apponint: cancelled_apponint.count,
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  salon_view_review(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, id } = body;

        await salon_view_review.validateAsync(body);
        await WriteReview.findAll({
          where: {
            users_id: `${id}`,
            salon_id: `${user_id}`,
          },
        });

        let order = await OrderOfUser.findOne({
          where: {
            id: `${id}`,
            is_walkin_user_appointment: null,
            order_status: 1,
          },
        });

        let view_review = await WriteReview.findAll({
          attributes: [
            "id",
            "price",
            "value",
            "quality",
            "friendliness",
            "cleanliness",
            "review_description",
          ],
          include: [{ model: User, attributes: ["id", "name"] }],
          where: {
            salon_id: `${user_id}`,
            users_id: `${order.users_id}`,
          },
        });

        return resolve(view_review);
      } catch (err) {
        return reject(err);
      }
    });
  }

  lat_longtitude(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id } = body;

        const latitude = 28.626137;
        const longitude = 79.821602;
        const distance = 2;

        const users = await User.scope({
          method: ["distance", latitude, longitude, distance],
        }).findAll({
          attributes: ["id"],
          where: {
            user_type: 2,
          },
          order: Sequelize.col("distance"),
          limit: 5,
        });

        return resolve(users);
      } catch (err) {
        return reject(err);
      }
    });
  }
}

module.exports = new UserService();

// let i;
// let DAY = [];
// let hour = [];
// for (let i = 1; i <= 7; i++) {
//   DAY.push({ day: i });

//   console.log("i", i);
//   const busshour = await SalonBusinessHours.findOne({
//     where: { salon_id: `${user_id}`, day: i },
//   });

//   if (busshour) {
//     hour.push({
//       day: busshour.day,
//       working_start_time: busshour.working_start_time,
//       working_end_time: busshour.working_end_time,
//       break_start_time: busshour.break_start_time,
//       break_end_time: busshour.break_end_time,
//     });
//   } else {
//     hour.push({
//       day: i,
//       working_start_time: " ",
//       working_end_time: " ",
//       break_start_time: " ",
//       break_end_time: " ",
//     });
//   }
// }
// let totaloforder = await OrderTotal.findAll({
//   where: {
//     salon_id: `${salon_id}`,
//     users_id: `${user_id}`,
//   },
// });
// console.log(totaloforder);
// if (totaloforder.length > 0) {
//   await OrderTotal.update(
//     { salon_id: `${salon_id}`, users_id: `${user_id}` },
//     {
//       where: {
//         salon_id: `${salon_id}`,
//         users_id: `${user_id}`,
//       },
//     }
//   );
// } else {
//   await OrderTotal.create({
//     salon_id: `${salon_id}`,
//     users_id: `${user_id}`,
//   });
// }

//address_salon
// let{lat,long}=body
//   await SalonUser.create({
//     lat:`${lat}`,long:`${long}`,

//   })
