const response = require("../../utils/response");
const statusCode = require("../../utils/statusCode");

const userService = require("../services/userService");

class DropdownController {
  async salon_register(req, res) {
    try {
      console.log(req.file);
      req.body.profile_pic = req.file.filename;
      let data = await userService.salon_register(req.body);
      response.success(
        res,
        "REGISTRATION",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`salon_register controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async salon_login(req, res) {
    try {
      let data = await userService.salon_login(req.body);
      response.success(
        res,
        "LOGIN_SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`salon_login controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_barber(req, res) {
    try {
      let data = await userService.add_barber(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`add_barber controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_barber(req, res) {
    try {
      let data = await userService.list_barber(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`list_barber controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async hasone(req, res) {
    try {
      let data = await userService.hasone(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`hasone controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_category(req, res) {
    try {
      let data = await userService.add_category(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`add_category controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async get_category(req, res) {
    try {
      let data = await userService.get_category();
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`get_category controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_service(req, res) {
    try {
      let data = await userService.add_service(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`add_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async get_service(req, res) {
    try {
      let data = await userService.get_service(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`get_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async edit_service(req, res) {
    try {
      let data = await userService.edit_service(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`edit_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async delete_service(req, res) {
    try {
      let data = await userService.delete_service(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`delete_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_service(req, res) {
    try {
      let data = await userService.list_service(req);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`list_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async user_register(req, res) {
    try {
      console.log(req.file);
      if (req.file) {
        req.body.profile_pic = req.file.filename;
      }
      let data = await userService.user_register(req.body);
      response.success(
        res,
        "REGISTRATION",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_register controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async user_login(req, res) {
    try {
      let data = await userService.user_login(req.body);
      response.success(
        res,
        "LOGIN_SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_login controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async forgot_password(req, res) {
    try {
      let data = await userService.forgot_password(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `forgot_password controller catch error ->> ${error.message}`
      );

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async sendotp(req, res) {
    try {
      let data = await userService.sendotp(req.body);
      response.success(
        res,
        "OTP_SENT",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`sendotp controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async verifyotp(req, res) {
    try {
      let data = await userService.verifyotp(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`verifyotp controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async new_password(req, res) {
    try {
      let data = await userService.new_password(req.body);
      response.success(
        res,
        "NEW_PASSWORD",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`new_password controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async reset_password(req, res) {
    try {
      let data = await userService.reset_password(req.body, req.headers);
      response.success(
        res,
        "NEW_PASSWORD",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`reset_password controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async belongsto(req, res) {
    try {
      let data = await userService.belongsto(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`belongsto controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async hasmany(req, res) {
    try {
      let data = await userService.hasmany(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`hasmany controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async belongstomany(req, res) {
    try {
      let data = await userService.belongstomany(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`belongstomany controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async refreshtoken(req, res) {
    try {
      let data = await userService.refreshtoken(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`refreshtoken controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_businesshours(req, res) {
    try {
      let data = await userService.add_businesshours(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `add_businesshours controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async edit_businesshours(req, res) {
    try {
      let data = await userService.edit_businesshours(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `edit_businesshours controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_weeklybusinesshour(req, res) {
    try {
      let data = await userService.list_weeklybusinesshour(
        req.body,
        req.headers
      );
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_weeklybusinesshour controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_dailybusinesshour(req, res) {
    try {
      let data = await userService.list_dailybusinesshour(
        req.body,
        req.headers
      );
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_dailybusinesshour controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async weeklydailyschedule(req, res) {
    try {
      let data = await userService.weeklydailyschedule(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `weeklydailyschedule controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_barberbusinesshours(req, res) {
    try {
      let data = await userService.add_barberbusinesshours(
        req.body,
        req.headers
      );
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `add_barberbusinesshours controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_barberdailyhours(req, res) {
    try {
      let data = await userService.list_barberdailyhours(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_barberdailyhours controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_barberweeklyhours(req, res) {
    try {
      let data = await userService.list_barberweeklyhours(
        req.body,
        req.headers
      );
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_barberweeklyhours controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async edit_userprofile(req, res) {
    try {
      let data = await userService.edit_userprofile(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `edit_userprofile controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async contactus(req, res) {
    try {
      let data = await userService.contactus(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`contactus controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async search(req, res) {
    try {
      let data = await userService.search(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`search controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_writereview(req, res) {
    try {
      let data = await userService.add_writereview(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `add_writereview controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_review(req, res) {
    try {
      let data = await userService.list_review(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`list_review controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_userfavouritesalon(req, res) {
    try {
      let data = await userService.add_userfavouritesalon(
        req.body,
        req.headers
      );
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `add_userfavouritesalon controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_userfavouritesalon(req, res) {
    try {
      let data = await userService.list_userfavouritesalon(
        req.body,
        req.headers
      );
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_userfavouritesalon controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_salondetail(req, res) {
    try {
      let data = await userService.add_salondetail(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `add_salondetail controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_salondetail(req, res) {
    try {
      let data = await userService.list_salondetail(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_salondetail controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_imagevideo(req, res) {
    try {
      console.log("array", req.file);
      let array = [];
      for (let j in req.files) {
        if (!array.includes(req.files[j].filename)) {
          array.push(req.files[j].filename);
          console.log("array", array[j]);
        }
      }

      //  array.forEach(function (value, i) {
      //     console.log( value);
      //     req.body.images_videos=[]
      //     if (!req.body.images_videos.includes(array[i])){
      //       req.body.images_videos.push(array[i])
      //     }

      // });
      req.body.images_videos = array;

      let data = await userService.add_imagevideo(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`add_imagevideo controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async edit_imagevideo(req, res) {
    try {
      console.log("array", req.file.filename);

      req.body.images_videos = req.file.filename;

      let data = await userService.edit_imagevideo(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `edit_imagevideo controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async delete_imagevideo(req, res) {
    try {
      let data = await userService.delete_imagevideo(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `edit_imagevideo controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_imagevideo(req, res) {
    try {
      let data = await userService.list_imagevideo(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_imagevideo controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async list_salons_detail(req, res) {
    try {
      let data = await userService.list_salons_detail(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(
        `list_salons_detail controller catch error ->> ${error.message}`
      );

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async referal_code(req, res) {
    try {
      let data = await userService.referal_code(req.body, req.headers);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`referal_code controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_order(req, res) {
    try {
      // console.log(req.body)
      // let array = [];
      // for (let j in req.body.salon_service_id) {
      //   if (!array.includes(req.files[j].filename)) {
      //     array.push(req.files[j].filename);
      //     console.log("array", array[j]);
      //   }
      // }
      // for (let j in req.body.salon_service_id) {
      //   if (!arr.includes(req.body.salon_service_id[j])) {
      //     array.push(req.body.salon_service_id[j]);
      //     console.log("array", arr[j]);
      //   }
      // }
      console.log("s", req.body.salon_service_id);

      let data = await userService.add_order(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`add_order controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async order_detail(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.order_detail(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`order_detail controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async date_time(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.date_time(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`date_time controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async order_cancelled_by_user_salon(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.order_cancelled_by_user_salon(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`order_cancelled_by_user_salon controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

 


  async user_appointment(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.user_appointment(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_appointment controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async salon_appointment(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.salon_appointment(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_appointment controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async user_appointment_completed(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.user_appointment_completed(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_appointment_completed controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async user_device_relation(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.user_device_relation(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_device_relation controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async salon_appointment_calender(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.salon_appointment_calender(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`salon_appointment_calender controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }
  

  async barber_appointment_calender(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.barber_appointment_calender(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`barber_appointment_calender controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async send_notification(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.send_notification(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`send_notification controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async add_notification(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.add_notification(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`add_notification controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }


  async delete_notification(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.delete_notification(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`delete_notification controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }


  async list_notification(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.list_notification(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`list_notification controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async cron_job(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.cron_job(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`cron_job controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async salon_earnings(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.salon_earnings(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`salon_earnings controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async salon_dashboard(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.salon_dashboard(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`salon_dashboard controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async user_dashboard(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.user_dashboard(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`user_dashboard controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }


  async salon_view_review(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.salon_view_review(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`salon_view_review controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }

  async lat_longtitude(req, res) {
    try {
      //  console.log('s',req.query.salon_service_id)

      let data = await userService.lat_longtitude(req.body);
      response.success(
        res,
        "SUCCESS",
        req.headers.language,
        data,
        statusCode.success,
        req.headers.is_openpgp
      );
    } catch (error) {
      console.log(`lat_longtitude controller catch error ->> ${error.message}`);

      if (error.message) {
        response.error(
          res,
          error.message,
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
        console.log(statusCode.error);
      } else
        response.error(
          res,
          error,
          req.headers.language,
          statusCode.error,
          req.headers.is_openpgp
        );
    }
  }
  
}

module.exports = new DropdownController();
// await listfavouritesalon.findAndCountAll({
//   attributes: ["is_favourite"],
//   include: [
//     {
//       model: SalonUser,
//       attributes: [
//         "id",
//         "salon_name",
//         "profile_pic",
//         "salon_type",
//         "service_gender",
//         "address",
//         "ratings",
//       ],
//     },
//   ],

//   where: {
//     users_id: `${user_id}`,
//     is_favourite: 1,
//   },
//   limit,
//   offset,
// })